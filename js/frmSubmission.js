// Add Member
var memberData = new Array();
var data = {};

$("#btnAddMember").on("click",function(event){

    if(($("#first_name").val()).trim()!="" && ($("#last_name").val()).trim()!="" && ($(".memberDOB").val()).trim()!=""){
      event.preventDefault();
      $('#frmAddMemberDiv :input').serializeArray().map(
        function(x){memberData[x.name] = x.value;
      });
      memberData.push({
        "first_name" : memberData.first_name,
        "last_name" : memberData.last_name,
        "gender" : memberData.gender,
        "dob" : memberData.dob,
      })
      var len = $(".adding_list li").length;
      $(".adding_list").append("<li>"+memberData.first_name+" "+memberData.last_name+"<a href='javascript:void(0);' class='deleteMember' onclick='deleteMember(this);' id="+len+"><i class='fa fa-close'></i> </a></li>");
    }else {
      bootbox.alert("Please enter memeber's details");
    }
});

$("#btnSubmit").on("click",function(event){
  var len=0;
  event.preventDefault();
  if($("#frmSignUp").valid())
  {
    // var step = $(this).parents(".signup_container").attr("id");
    // $("#"+step).addClass("disable_step");
    var formdata = $("#frmSignUp").serializeArray().map(
        function(x){data[x.name] = x.value;
     });
     data['members'] = memberData;

    $.ajax({
  		type: "POST",
  		url: "api/patient/register",
  		cache: false,
  		data: '{"data": '+JSON.stringify(data)+'}',
  		dataType: "json",
  		beforeSend: function(data){
  			// ajaxProcessLoader(element,"ON");
  		},
  		success: function(response){
        if(response.status=="1"){
          bootbox.alert(response.message,function(){
              window.location.href = 'http://letsnurture.co.uk/demo/emacs/signin';
          });

        }
  		}
  	});
  }
  else {
    id = $("#frmSignUp").find(".error:visible").parents(".signup_container").attr("id");
    len = $("#frmSignUp").find(".error:visible").parents(".signup_container").length;
    console.log(len);
    $("#"+id).removeClass("disable_step");
    if(id=="step_2" && len<2){
      setTimeout(function(){
        $("#step_1").addClass("disable_step");
      },200);
    }
  }
});

function deleteMember(obj){
  var index = $(obj).attr("id");
  memberData.splice(index,1);
  $(obj).parent('li').remove();
  // data['members'].remove(index);
  var child=1;
  var len = $(".adding_list li").length;
  for(var i=0;i<=len;i++){
    $(".adding_list li:nth-child("+child+") a").attr("id",i)
    child = child + 1;
  }
  //console.log(memberData);
}

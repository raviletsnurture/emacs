jQuery.validator.addMethod("noSpace", function(value, element) {
  return value.indexOf(" ") < 0 && value != "";
}, "No space please and don't leave it empty");

$("#frmSignUp").validate({
    onfocusout: false,
      invalidHandler: function(form, validator) {
          var errors = validator.numberOfInvalids();
          if (errors) {
              validator.errorList[0].element.focus();
          }
      },
    ignore: [],
    debug: false,
    rules: {
      insurance_provider_name:{
        required:"#has_insurance:checked",
      },
      policy_id:{
        required:"#has_insurance:checked",
      },
      insurance_member_id:{
        required:"#has_insurance:checked",
      },
      firstname:{
        required: true,
        noSpace: true,
        minlength:3
      },
      lastname:{
        required: true,
        noSpace: true,
        minlength:3
      },
      password:{
        required: true,
        minlength:5
      },
      confirmPassword:{
        equalTo: "#password"
      },
      address_line1:{
        required:true,
      },
      city:{
        required: true,
      },
      country:{
        required: true,
      },
      zip:{
        required: true,
      },
      email:{
        required: true,
        email: true
      },
    },
    messages:{
      firstname:{
        required: 'Please enter first name',
      },
      lastname:{
        required: 'Please enter last name',
      },
      password:{
        required: 'Please enter password',
      },
      confirmPassword:{
        equalTo: 'Confirm password does not match',
      },
      address_line1:{
        required: 'Please enter address line 1',
      },
      insurance_member_id:{
        required: 'Please enter insurance member id'
      },
      city:{
        required: 'Please enter city'
      },
      country:{
        required: 'Please select country'
      },
      zip:{
        required: 'Please enter zip'
      },
      email:{
        required: 'Please enter email',
      },
    },
  });

$("#btnSubmit").on("click",function(){
  var id;
  if($("#frmSignUp").valid())
  {

  }
  else {
    $(".login_high_width div").removeClass("disable_step");
   var id = $('.error:visible').first().parents(".signup_container").attr("id");
   $("#"+id).removeClass("disable_step");
 }
});


$("#email").on("change",function(){
  var email = $(this).val();
  var obj = $(this);
  var dataString = "email="+email;
  $.ajax({
    type:"POST",
    data:dataString,
    url:'isEmailExist',
    beforeSend:function(){

    },
    success:function(data){
      // console.log(data);
      if(data=="1"){
        $(obj).val("");
        bootbox.alert("Email already exist");
        $(obj).addClass("error");
      }else {
        $(obj).removeClass("error");
      }
    }
  });
});

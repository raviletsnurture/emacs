var host = $("#host").val();
var remote = "";
var id = "";
var old_content = "";
var url = "";
var element = "";

function set_modal_form_content(data, parent){
  //console.log(data);
  $.each(data,function( key, value ) {

	  if(key == "page_content" && $(".page_content").length != 0){
		  $('.page_content').data("wysihtml5").editor.setValue(value);
	  }

    if(key == "testimonial_user_image_url" && value != "" ){
      var newSrc = $('#formViewImage').attr('src')+value;
      $('#formViewImage').attr('src',newSrc).show();
    }
        $(parent+" ."+key).val(value);

  });
}

function ajaxProcessLoader(element,status){
  old_content = $(element).html();

  if(status == "ON"){
    $(element).children().hide();
    $(element).addClass("relative");
    $(element).append("<img src='"+ host +"/images/progress.gif' class='ajaxLoader'>");
    $(element).find(".ajaxLoader").show();
    $(element).prop("disabled","true");
  } else if(status == "OFF"){
    $(element).removeClass("relative");
    $(element).find('.ajaxLoader').remove();
    $(element).children().show();
    $(element).removeProp("disabled");
  }
}

﻿$(document).ready(function () {
  var curl = window.location.href;

  $(".widget-menu li.active").removeClass("active");
  $(".widget-menu a[href='"+curl+"']").parent().addClass("active");
});

$(document).on("change",".shortfield",function(){
  var shortcode = $(this).children(":selected").val();
  $(".shortcode").val(shortcode);
});

$(document).on("click",".shortcode",function(e){
  //var shortcode = $(this).children(":selected").val();
  $(this).select();
  //$(this).execCommand("copy");
});

$(document).on("click","a[data-target=#AddFormModal]",function(){

	if($(".page_content").length != 0){
		$('.page_content').data("wysihtml5").editor.setValue("");
	}

  
  element = $(this);
  $(this+" .AddForm")[0].reset();
  $('#AddFormModal').attr("data-remote",$(element).attr("data-new-remote"));
  $('#AddFormModal').removeAttr("data-id");
});

$(document).on("click","a[data-target=#EditFormModal]",function(){

  remote = $(this).attr("data-remote");
  id = $(this).attr("data-id");
  modal_title = $(this).attr("data-modal-title");
  element = $(this);

  url = host+remote+'/'+id;

  $.ajax({
		type: "GET",
		url: url,
		cache: false,
    dataType: "json",
		beforeSend: function(data){
			ajaxProcessLoader(element,"ON");
		},
		success: function(response){
      ajaxProcessLoader(element,"OFF");

      if(response.status){
        $('#AddFormModal').modal('show');
        $('#AddFormModal').attr("data-remote",$(element).attr("data-remote"));
        $('#AddFormModal').attr("data-id",$(element).attr("data-id"));
      }

      set_modal_form_content(response.data,"#AddFormModal");
		}
	});
});

$(document).on("click","a[data-target=#Delete]",function(){
  ajaxProcessLoader(element,"ON");
  remote = $(this).attr("data-remote");
  id = $(this).attr("data-id");
  modal_title = $(this).attr("data-modal-title");
  element = $(this);
  if(confirm("Are you sure want to delete this record.?")){
    $.ajax({
      type: "DELETE",
      url: host+remote+'/'+id,
      cache: false,
      dataType: "json",
      beforeSend: function(data){
        ajaxProcessLoader(element,"ON");
      },
      success: function(response){
        ajaxProcessLoader(element,"OFF");
        if(response.status){
          alert("Record Deleted Successfully");
          window.location.reload();
        }
      }
    });
  }
});

$(document).on("click","button.btnSaveRecord",function(){

  remote = $('#AddFormModal').attr("data-remote");

  id = "";
  url = host+remote;
  element = $(this);
  var type = "POST";
  if(typeof $('#AddFormModal').attr("data-id") !== typeof undefined && $('#AddFormModal').attr("data-id") !== false && $('#AddFormModal').attr("data-id") != ""){
    id = $('#AddFormModal').attr("data-id");
    url = url+'/'+id;
    type = "PUT";
  }
  var data = {};
  var formdata = $("#AddFormModal .AddForm").serializeArray().map(
                function(x){data[x.name] = x.value;
             });

  console.log(data);

  $.ajax({
		type: type,
		url: url,
		cache: false,
		data: '{"data": '+JSON.stringify(data)+'}',
		dataType: "json",
		beforeSend: function(data){
			ajaxProcessLoader(element,"ON");
		},
		success: function(response){


			ajaxProcessLoader(element,"OFF");
		      if(response.status == "1"){
		        alert(response.message);
		        window.location.reload();
		      } else {
		        alert(response.message);
		    }
		}
	});
});

$(document).on("click","button.btnSaveAllRecord",function(){

	  remote = $('#AddFormModal').attr("data-remote");
	  id = "";
	  url = host+remote;
	  element = $(this);
	  var type = "POST";
	  if(typeof $('#AddFormModal').attr("data-id") !== typeof undefined && $('#AddFormModal').attr("data-id") !== false && $('#AddFormModal').attr("data-id") != ""){

      id = $('#AddFormModal').attr("data-id");


      if(remote = "/admin/testimonial"){
        url = url+'/update/'+id;
        type = "POST";
      }
      else{
        url = url+'/'+id;
  	    type = "PUT";
      }

    }
	  var data = {};

	  var frm = $("#AddFormModal .AddForm").serializeArray().map(
              function(x){data[x.name] = x.value;
              });
	  data.tempath = $('#testimg')[0].files[0];

	  var formData = new FormData($("#AddFormModal .AddForm")[0]);

	  console.log(formData);
	  //alert(formData);

	  $.ajax({
			type: type,
			url: url,
			cache:false,
			async:true,
			processData: false,
			contentType: false,

			data: formData,
			//dataType: "json",
			//contentType:"multipart/form-data",
			//contentType:false,
			//processData:false,
			beforeSend: function(data){
				ajaxProcessLoader(element,"ON");
			},
			success: function(response){

				/*if(remote = "/admin/testimonial"){
					alert('please upload');
				}*/

				ajaxProcessLoader(element,"OFF");
			      if(response.status == "1"){
			        alert(response.message);
			        window.location.reload();
			      } else {
			        alert(response.message);
			    }
			}
		});
	});

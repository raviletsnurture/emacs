<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model
{
  protected $table = 'em_medicines';
  protected $primaryKey = 'medicine_id';


 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['prescription_id','medicine_name','slot','time_to_take'];

 /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
  protected $hidden = ['created_at','updated_at'];

  public function post()
  {
      return $this->belongsTo('App\Prescription', 'prescription_id', 'prescription_id');
  }

}

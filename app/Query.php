<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
     protected $table = 'em_query';
     protected $primaryKey = 'msg_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['msg_type','user_id','query_title','text','parent_id','member_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
}

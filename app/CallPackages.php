<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallPackages extends Model
{

     protected $table = 'em_packages';
     protected $primaryKey = 'package_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['package_price','package_duration','package_name'];

  //  protected $fillable = ['package_price','package_duration','package_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorSpeciality extends Model
{
  protected $table = 'em_doctor_type';
  protected $primaryKey = 'doctor_type_id';


  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['doctor_specialization'];

  /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
  protected $hidden = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
     protected $table = 'em_testimonial';
     protected $primaryKey = 'testimonial_id';

	protected $_wysihtml5_mode = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = ['testimonial_text','testimonial_by_user_name','testimonial_user_image_url','testimonial_by_post'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}

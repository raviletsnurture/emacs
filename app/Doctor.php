<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
     protected $table = 'em_doctor';
     protected $primaryKey = 'doctor_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['user_id','doctor_phone_number','doctor_speciality','is_certified','is_available_now','doctor_avtar',];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
      protected $hidden = ['created_at','updated_at'];

      public function userDetails() // 0 = Admin || 1 = Doctor || 2 = user
      {
        return $this->belongsTo('App\User','user_id','user_id');
      }
  }

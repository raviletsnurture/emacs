<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
     protected $table = 'em_members';
     protected $primaryKey = 'member_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['parent_id','first_name','last_name','dob','gender'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at'];

    public function ofpatient() // 0 = Admin || 1 = Doctor || 2 = user
    {
      return $this->belongsTo('App\Patient','user_id','parent_id');
    }
}

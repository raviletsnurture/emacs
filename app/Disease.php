<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
   	 protected $table = 'em_diseases';
     protected $primaryKey = 'disease_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['disease_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}

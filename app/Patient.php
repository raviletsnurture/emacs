<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
     protected $table = 'em_patient';
     protected $primaryKey = 'patient_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['user_id','disease_name','patient_phone','has_insurance','insurance_provider_name','policy_id','insurance_member_id','insurance_card_image','credit_card_image','have_members'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at','patient_id','user_id','patient_type'];

    public function userDetails() // 0 = Admin || 1 = Doctor || 2 = user
    {
      return $this->belongsTo('App\User','user_id','user_id');
    }

    public function members() // 0 = Admin || 1 = Doctor || 2 = user
    {
      return $this->hasMany('App\Member','parent_id','user_id');
    }

}

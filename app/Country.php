<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
  protected $table = 'em_country';
  protected $primaryKey = 'country_id';


 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['country_name','country_status'];

  public function getAll(){
    dd(this);
  }
}

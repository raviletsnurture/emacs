<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medication extends Model
{
    protected $table = 'em_medicines_history';
    protected $primaryKey = 'medicine_id';


    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['user_id','disease_name','dosage','taking_since'];

    /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
    protected $hidden = ['created_at','updated_at'];


}

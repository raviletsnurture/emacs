<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
  protected $table = 'em_prescription';
  protected $primaryKey = 'prescription_id';


 /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = ['patient_id','doctor_id','prescription_title'];

 /**
  * The attributes excluded from the model's JSON form.
  *
  * @var array
  */
  protected $hidden = ['created_at','updated_at'];

  public function medicines()
  {
      return $this->hasMany('App\Medicine', 'prescription_id', 'prescription_id');
  }
}

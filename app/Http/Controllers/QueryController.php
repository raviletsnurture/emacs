<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Query as Query;
use App\Patient as Patient;
use DB;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //  dd('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($_POST) && !empty($_POST)){
            $data = $_POST;
          }else {
            $data = $request->json()->get('data');
          }
          try{
              $response = [ 'data' => [] ];
              $statusCode = 200;
              $optionalRules = array();

              $rules = array(
                  'msg_type' => 'required',
              	  'user_id'  => 'required',
                  'text'  => 'required|max:1000',
              );

              $validator = Validator::make($data, array_merge($rules, $optionalRules));

              if(!$validator->fails()) {

                if($query = Query::create($data))
                {
                    if($query->save()){
                      $response['status'] = "1";
                      $response['data'] = Query::find($query->msg_id);
                      unset($response['data']['created_at']);
                      unset($response['data']['updated_at']);
                      $response['message'] = "Query added successfully. !!";
                    }
                }

              } else {
                $response['status'] = "0";
                $response['data'] = array();
                $response['message'] = $validator->errors()->first();
              }
          } catch (Exception $e){
              $statusCode = 400;  // Bad Request Error Code
                $response['status'] = 0;
                $response['message'] = "Bad Request Error";
          } finally{
              return response()->json($response, $statusCode);
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * List of previous queries
     *
     * @param  int  $user_id
     * @return List json array
     * @created_at 2-2-2016
     * @by nilesh
     */
    public function queryList(Request $request,Guard $auth)
    {
      $data = $request->json()->get('data');

      if(isset($data['_token']) && $data['_token'] == "ws"){


        $user_id = $data['user_id'];
        $query = User::find($user_id);

        if($query){
          if(Query::where('user_id','=',$user_id)->count() > 0){
              $returnData['status'] = "1";
              $returnData['message'] = "Success";
              $returnData['data'] = Query::where('user_id','=',$user_id)->get();
              echo json_encode($returnData);exit;
          }
          else{
              $returnData['status'] = "0";
              $returnData['message'] = "No Queries found fot this user.";
              echo json_encode($returnData);exit;
          }

        }
        else{
          $returnData['status'] = "0";
          $returnData['message'] = "User does not exists. Please check and provide proper details.";
          echo json_encode($returnData);exit;
        }
      }elseif(isset($auth->user()->user_id) && !empty($auth->user()->user_id)){

        $user_id = $auth->user()->user_id;
        $query = User::find($user_id);
        $memberData = array();

        if(isset($query) &&  !empty($query)){
            if($auth->user()->user_type=="1"){
              $returnData = Query::orderBy('created_at','DESC')->get();
            }else {
              $returnData = Query::where('user_id','=',$user_id)->orderBy('created_at','DESC')->get();

              if(Patient::where('user_id','=',$user_id)->with('members')->first() != "")
              {
              	$memberData = Patient::where('user_id','=',$user_id)->with('members')->first()->members;
              }

            }
            if($auth->user()->user_type=="1"){
              return view('pages.doctor.inquiryList',compact('returnData','memberData'));
            }else {
              return view('pages.patient.inquiryList',compact('returnData','memberData'));
            }
        }
        else{
            return abort(404, 'Unauthorized action.');
        }
      }
      else{

        $returnData['status'] = "0";
        $returnData['message'] = "Parameter missing. Please add token";
        echo json_encode($returnData);exit;
      }




    }
    public function queryDetails(){
      extract($_POST);
      if(Query::find($query_id)->count()>0){

        $queryData = DB::table('em_query')
        ->select('replyer.firstname as replyerFName','replyer.lastname as replyerLName','em_users.firstname','em_users.lastname','em_query.*','replyObj.user_id', 'replyObj.text as reply')
        ->where('em_query.msg_id','=',$query_id)
        ->leftJoin('em_query as replyObj','replyObj.parent_id','=','em_query.msg_id')
        ->leftJoin('em_users','em_users.user_id','=','em_query.user_id')
        ->leftJoin('em_users as replyer','replyer.user_id','=','replyObj.user_id')->get();

        //dd($queryData);

        $html = view('pages.doctor.inqueryAjaxDetail',compact('queryData'))->render();
        return response()->json(array('success' => true,'html'=>$html));
      }else {
        exit("0");
      }
    }
}

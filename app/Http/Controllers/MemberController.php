<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Member as Member;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->json()->get('data');
        
    try{
            $response = [ 'data' => [] ];
            $statusCode = 200;
            $optionalRules = array();

            $rules = array(
                'parent_id' => 'required',
            	'gender'  => 'required',
                'first_name'  => 'required|max:30',
            	'last_name'  => 'required|max:30',
            
            );

            $validator = Validator::make($data, array_merge($rules, $optionalRules));

            if(!$validator->fails()) {
				$data['dob'] = date("Y-m-d",strtotime($data['dob']));
				
              if($query = Member::create($data))
              {
                  if($query->save()){
                    $response['status'] = "1";
                    $response['data'] = Member::find($query->member_id);
                    
                    $response['message'] = "Member added successfully. !!";
                  }
              }

            } else {
              $response['status'] = "0";
              $response['data'] = array();
              $response['message'] = $validator->errors()->first();
            }
        } catch (Exception $e){
            $statusCode = 400;  // Bad Request Error Code
              $response['status'] = 0;
              $response['message'] = "Bad Request Error";
        } finally{
            return response()->json($response, $statusCode);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

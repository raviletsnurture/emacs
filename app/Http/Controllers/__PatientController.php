<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Validator;
use App\User;
use App\Patient;
use App\Country;
use App\Member as Member;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DiseaseInfo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Auth;
use Mail;
use Crypt;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(isset(Auth::user()->user_id)){
          $userid = Auth::user()->user_id;
          return redirect('patient/detail/'.$userid);

          //['user.preferences.update', Auth::user()->id]
        }
        else{
          return " patient page";
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "user/create";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $data = $request->json()->get('data');

      if(!isset($data['password']) || $data['password'] == "") $data['password'] = $data['username'];

      try{
          $response = array();
          $statusCode = 200;
          $optionalRules = array();

          $rules = array(
              'username' => 'required|max:30',
              'firstname' => 'required|max:30',
              'email' => 'required|email|max:100|unique:em_users,email',
              'user_type' => 'required'
          );

          $validator = Validator::make($data, array_merge($rules, $optionalRules));

          if(!$validator->fails()) {

            if(isset($data['password']) && $data['password'] != "") $data['password'] = Hash::make($data['password']);

            if($user = User::create($data))
            {
                if($user->save()){
                  $response['status'] = "1";
                  $response['data'] = User::find($user->user_id);
                  $response['message'] = "Registration Successfull. !!";
                }
            }

          } else {
            $response['status'] = "0";
            $response['data'] = array();
            $response['message'] = $validator->errors()->first();
          }
      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
            $response['status'] = 0;
            $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = User::find($id);

          if($query)
          {
            $user = $query->toArray();  // get array of attributes->values from User Obj
            $response['data'] = $user;  // Set Response Obj


            $response['data']['patient'] = $query->patient;
            if($query->patient){
                $response['data']['patient']['members'] = Patient::find($query->patient->patient_id)->members;
            }


            $response['status'] = 1;

          } else {

            $response['message'] = "Requested User Not Found";  // Set Response Obj
            $response['status'] = 0;
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
          return response()->json($response, $statusCode);
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      if(!empty($request->json()->get('data')))
      {
         $data = $request->json()->get('data');
  	     if($data['_token'] == "ws")
      		{

            $response = array();
            $statusCode = 200;
            $optionalRules = array();

            $rules = array(
                  'firstname' => 'max:30',
                  'lastname' => 'max:30',
                  'password' => 'max:30',
                  'gender' => 'max:30',
                  'address_line1' => 'max:200',
                  'city' => 'max:100',
                  'country' => 'max:100',
                  'zip' => 'max:15',
                  'email' => 'required|email|max:100|unique:em_users,email,'.$id.',user_id'

            );

            $validator = Validator::make($data, array_merge($rules, $optionalRules));

            if(!$validator->fails())
            {

              $query = User::find($id);

              //print_r($query->count()); exit;
              if($query)
              {
                $user = $query;

                // foreach($data as $key => $value)
                // {
                //   $user->$key = $value;
                // }

                if($query->update($data, $id))
                {
                  unset($data['user_id']);

                  $patientQuery = Patient::where('user_id', '=', $id)->first();
                  $pId = $patientQuery['patient_id'];

                  if($patientQuery->update($data, $pId))
                  {


                    if($patientQuery['have_members'] == "1")
                    {
                      $memata = array();
                      foreach($data['members'] as $members)
                      {
                        $currentMEmberId = $members['member_id'];
                        $memberData = Member::where('member_id', '=', $currentMEmberId)->first();
                        $memberData->update($members, $currentMEmberId);

                        array_push($memata, $members);
                      }
                      $response['status'] = "1";
                      $response['data'] = $user;
                      $response['data']['patient'] = $patientQuery;
                      $response['message'] = "Updation Successfull";
                      $response['data']['patient']['members'] = $memata;
                    }



                    echo json_encode($response);exit;

                  }


                }

              }
              else{
                $response['status'] = "0";
                $response['data'] = array();
                $response['message'] = "User does not exists.";
                echo json_encode($response);exit;
              }


            }
            else{
              $response['status'] = "0";
              $response['data'] = array();
              $response['message'] = $validator->errors()->first();
              echo json_encode($response);exit;
            }

          }
      }
      else{
        $returnData['status'] = "0";
        $returnData['message'] = "Parameter missing.";
        echo json_encode($returnData);exit;
      }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = User::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfull";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No User Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    public function verify($string = "", $email = "", Request $request)
    {
      $data['string'] = $string;
      $data['email'] = $email;

      $validator = Validator::make($data, [
          'email' => 'email|exists:em_users,email',
          'string' => 'required'
      ],array('string','Token Required.'));

      if(!$validator->fails()) {
        if(User::where('email','=',$data['email'])->count())
        {
          $user = User::where('email','=',$data['email'])->first();

          if(substr(md5($user->user_id.$user->created_at),0,8) == $string)
          {
            $user->is_verified = 1;
            $user->is_blocked = 0;
            $user->status = 1;

            if($user->save())
            {
              $request->session()->put('flash_message','Verified Successfully');
              return redirect('patient');
            } else {
              $request->session()->put('flash_message_error','Server Error. Please try later.');
              return redirect('patient');
            }
          } else {
            $request->session()->put('flash_message_error','Token Mismatch. Please try to signup again.');
            return redirect('patient');
          }
        }
      } else {
        return back()->withErrors($validator);
      }
    }

    /**
     * User register form
     *
     */
    public function registerPatient()
    {

        $diseaseObj = new DiseaseInfo;
        $countryObj = new Country;

        $disease = $diseaseObj->getall();
        $countryData = this->countryList();

        dd($countryData);
        return view('pages.signup')->with('disease', $disease);
    }

    /**
     * Will list all country list
     *
     */
    public function countryList()
    {

      $countryData = $users = DB::table('em_countries')->get();
      $response['status'] = "1";
      $response['message'] = "Success";
      $response['data'] = $countryData;
      echo json_encode($response);exit;

    }


    /**
     * Will list all insurace provide list
     *
     */
    public function providersList()
    {

      $provideData = $users = DB::table('em_insurance_providers')->get();

      $response['status'] = "1";
      $response['message'] = "Success";
      $response['data'] = $provideData;
      echo json_encode($response);exit;

    }

    /**
     * Forgot pass first   event to send email to user
     *
     */
    public function changePass(Request $request)
    {
        $diseaseObj = new DiseaseInfo;
        $disease = $diseaseObj->getall();

        return view('pages.signup')->with('disease', $disease);
    }

    /**
     * Forgot pass first   event to send email to user
     *
     */
    public function sendResetPAssLink(Request $request)
    {
    	$data = input::all();
    	$email = $data['email'];
        $query = User::where('email', $email)->count();

        if($query)
        {
            $message_body = "This is teszting conetnnsm sdfmnsdf osdf lskdf";
	        $toEmail = "nilesh.letsnurture@gmail.com";
	        $userName = "User";

	        $mail = Mail::raw($message_body, function($message) use ($toEmail,$userName) {

	            $message->to((string) $toEmail, (string) $userName)->subject('Emacs - Forgot pass');

	        });

	        if(isset($mail)){
	        	$response['status'] = "1";
              	$response['message'] = "Please check your mail for reset password link";
	        }

        }
    	else
    	{
	    	$response['status'] = "0";
            $response['message'] = "Email does not exist in our system. Please enter your registered email.";
	    }

    }

    /**
     * Forgot pass first   event to send email to user
     *
     */
    public function memberList(Request $request)
    {

      if(!empty($request->json()->get('data')))
      {
          $wsData = $request->json()->get('data');
          $parent_id = $wsData['parent_id'];

          $response = [ 'data' => [] ];
          if(Member::where('parent_id','=',$parent_id)->count() > 0){
            $returnData['status'] = "1";
            $returnData['message'] = "Success";
            $returnData['data'] = Member::where('parent_id','=',$parent_id)->get();
            echo json_encode($returnData);exit;
          }
          else{
            $response['status'] = "0";
            $response['message'] = "No members found.";
            echo json_encode($response);exit;
          }

      }

    }


    /**
     * addPatient
     *
     */
    public function addPatient(Request $request)
    {

      $data = input::all();

      if(!empty($request->json()->get('data')))
      {
      		$wsData = $request->json()->get('data');


    		if($wsData['_token'] == "ws")
    		{


	    		$response = array();
		        $statusCode = 200;
		        $optionalRules = array();

		        $rules = array(
                  'firstname' => 'required|max:30',
		              'lastname' => 'required|max:30',
                  'password' => 'required|max:30',
                  'gender' => 'required|max:30',
                  'address_line1' => 'required|max:200',
                  'city' => 'required|max:100',
                  'country' => 'required|max:100',
                  'zip' => 'required|max:15',
		              'email' => 'required|email|max:100|unique:em_users,email'
		        );

		        $validator = Validator::make($wsData, array_merge($rules, $optionalRules));
		        if($validator->fails())
			      {
		        	//$erroData = json_encode($validator->errors()->all()));
		        	$returnData['status'] = "0";
		        	$returnData['message'] = $validator->errors()->first();


		        	echo json_encode($returnData);
		        	exit();

			         }
			        else{
				    if(isset($wsData['password']) && $wsData['password'] != "") $wsData['password'] = Hash::make($wsData['password']);

		            if($user = User::create($wsData))
		            {

		                if($user->save())
		                {

                      $wsData['user_id'] = $user->user_id;
                      if($patient = Patient::create($wsData))
                      {
                          if($patient->save())
                          {

                            $returnData['status'] = "1";
                            $returnData['message'] = "Patient registered successfully.";
                            $returnData['data'] = User::find($user->user_id);
                            $returnData['data']['patient'] = $returnData['data']->patient;

                            if($wsData['have_members'] == "1"){
                              $members = $wsData['members'];


                              foreach ($members as $member){
                                $member['parent_id'] = $wsData['user_id'];
                                $memberObj = Member::create($member);
                                $memberObj->save();
                              }


                              if($returnData['data']->patient){
                                  $returnData['data']['patient']['members'] = Patient::find($returnData['data']->patient->patient_id)->members;
                              }


                            }

                              unset($returnData['data']['created_at']);
                              unset($returnData['data']['updated_at']);
                              echo json_encode($returnData);
                              exit();
                          }
                      }


		                }
		                else{
		                	$returnData['status'] = "0";
				        	$returnData['message'] = "Some thing went wrong try after some time";

				        	echo json_encode($returnData);
				        	exit();
		                }
		            }
				}
    		}
      }
    //  if(!isset($data['password']) || $data['password'] == "") $data['password'] = $data['firstname'];

      try{
          $response = array();
          $statusCode = 200;
          $optionalRules = array();

          $rules = array(
              'username' => 'required|max:30',
              'firstname' => 'required|max:30',
              'email' => 'required|email|max:100|unique:em_users,email',
              'user_type' => 'required'
          );

          $validator = Validator::make($data, array_merge($rules, $optionalRules));

          if(!$validator->fails()) {

            if(isset($data['password']) && $data['password'] != "") $data['password'] = Hash::make($data['password']);

            if($user = User::create($data))
            {
                if($user->save()){
                  //Redirect::view('signin')->with('message', 'Registration Successfull. !!');
                  return Redirect::to('signin')->with('message', 'Registration Successfull. !!');
                }
            }

          } else {
              //dd($validator->errors()->all());
            return Redirect::back()->withErrors([$validator->errors()->all()]);
          }
      } catch (Exception $e){
            return Redirect::back()->withErrors(['msg', 'Bad Request Error']);
      }
    }

}

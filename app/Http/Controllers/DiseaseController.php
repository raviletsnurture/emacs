<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Disease as Disease;

class DiseaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $types = Disease::all();
       return view('pages.admin.disease')->withTypes($types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $value = array('df','sdf');
        echo json_encode($value);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $data = $request->json()->get('data');
      if($data['_token'] == "ws"){

        try{
            $response = [ 'data' => [] ];
            $statusCode = 200;
            $optionalRules = array();

            $rules = array(
                'disease_name' => 'required|max:50|unique:em_diseases,disease_name'
            );

            $validator = Validator::make($data, array_merge($rules, $optionalRules),array('disease_name.unique'=>'Already exists.'));

            if(!$validator->fails()) {


              if($disease = Disease::create($data))
              {

                  if($disease->save()){

                    $response['status'] = "1";
                    //$response['data'] = Disease::find($disease->disease_id);
                    $response['message'] = "Disease added successfully. !!";

                  }
              }

             else {
              $response['status'] = "0";
              $response['data'] = array();
              $response['message'] = $validator->errors()->first();
            }
          }
        }
        catch (Exception $e){
            $statusCode = 400;  // Bad Request Error Code
              $response['status'] = 0;
              $response['message'] = "Bad Request Error";
        }
        finally{
            echo json_encode($response);
            exit;
        }

      }

      else{
          try{
              $response = [ 'data' => [] ];
              $statusCode = 200;
              $optionalRules = array();

              $rules = array(
                  'disease_name' => 'required|max:50|unique:em_diseases,disease_name'
              );

              $validator = Validator::make($data, array_merge($rules, $optionalRules),array('disease_name.unique'=>'Already exists.'));

              if(!$validator->fails()) {

                if($disease = Disease::create($data))
                {
                    if($speciality->save()){
                      $response['status'] = "1";
                      $response['data'] = Disease::find($disease->disease_id);
                      $response['message'] = "Disease added successfully. !!";
                    }
                }

              } else {
                $response['status'] = "0";
                $response['data'] = array();
                $response['message'] = $validator->errors()->first();
              }
          } catch (Exception $e){
              $statusCode = 400;  // Bad Request Error Code
                $response['status'] = 0;
                $response['message'] = "Bad Request Error";
          } finally{
              return response()->json($response, $statusCode);
          }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = Disease::find($id);

          if($query)
          {
            $speciality = $query->toArray();  // get array of attributes->values from User Obj
            $response['data'] = $speciality;  // Set Response Obj
            $response['status'] = 1;
          } else {
            $response['message'] = "Requested Speciality Not Found";  // Set Response Obj
            $response['status'] = 0;
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
          return response()->json($response, $statusCode);
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = $request->json()->get('data');

      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;

          $validator = Validator::make($data, [
           'disease_name' => 'required|max:50'
          ]);

          if(!$validator->fails()) {
            $query = Disease::find($id);

            //print_r($query->count()); exit;
            if($query)
            {
              $user = $query;

              foreach($data as $key => $value)
              {
                $user->$key = $value;
              }

              if($user->save())
              {
                $response['status'] = "1";
                $response['data'] = $user;
                $response['message'] = "Updation Successfull";
              }
            }

          } else {
            $response['status'] = "0";
            $response['data'] = array();
            $response['message'] = $validator->errors()->first();
          }
      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = Disease::find($id);

          if($query)
          {
            if($query->delete())
            {
              $response['status'] = "1";
              $response['message'] = "Removed Successfull";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Record Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * List all disease
     *
     * 
     */
    public function diseaseList()
    {
      $types = Disease::all();
      echo json_encode($types);exit;
    }


}

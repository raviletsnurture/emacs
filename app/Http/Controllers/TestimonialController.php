<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Support\Facades\Input;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       $types = Testimonial::all();
       return view('pages.admin.testimonial')->withTypes($types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = Input::all();

	      try{
	          $response = [ 'data' => [] ];
	          $statusCode = 200;
	          $optionalRules = array();

	          $rules = array(
	              'testimonial_text' => 'required|max:1000',
	              'testimonial_by_user_name'=> 'required|max:50'
	          );

	          $validator = Validator::make($data, array_merge($rules, $optionalRules));

	          if(!$validator->fails()) {

      	          if (Input::hasFile('tempath'))
      				    {
      				    $file = Input::file('tempath');
      		      		$tmpFilePath = '/images/upload/';
      		      		$tmpFileName = time().$file->getClientOriginalName();
      		      		$file = $file->move(base_path().$tmpFilePath, $tmpFileName);
      		      		$path = $tmpFilePath . $tmpFileName;
                    $data['testimonial_user_image_url'] = $tmpFileName;
                    unset($data['tempath']);


      		      		if($speciality = Testimonial::create($data))
      		            {
      		                if($speciality->save()){
      		                  $response['status'] = "1";
      		                  $response['data'] = Testimonial::find($speciality->doctor_type_id);
      		                  $response['message'] = "Testimonial added successfully. !!";
      		                }
      		            }
      	            }
      				else{
      				    $response['status'] = "0";
      	            	$response['data'] = array();
      	           	 	$response['message'] = "Please upload user image.";
      				}

	          } else {
	            $response['status'] = "0";
	            $response['data'] = array();
	            $response['message'] = $validator->errors()->first();
	          }
	      } catch (Exception $e){
	          $statusCode = 400;  // Bad Request Error Code
	            $response['status'] = 0;
	            $response['message'] = "Bad Request Error";
	      } finally{
	          return response()->json($response, $statusCode);
	      }
  	  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = Testimonial::find($id);

          if($query)
          {
            $speciality = $query->toArray();  // get array of attributes->values from User Obj
            $response['data'] = $speciality;  // Set Response Obj
            $response['status'] = 1;
          } else {
            $response['message'] = "Requested Testimonial Not Found";  // Set Response Obj
            $response['status'] = 0;
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
          return response()->json($response, $statusCode);
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $data = Input::all();
      unset($data['_token']);
      try{
          $response = [ 'data' => [] ];
          $statusCode = 200;

          $optionalRules = array();

          $rules = array(
              'testimonial_text' => 'required|max:1000',
              'testimonial_by_user_name'=> 'required|max:50'
          );

          $validator = Validator::make($data, array_merge($rules, $optionalRules));

          if(!$validator->fails()) {
            $query = Testimonial::find($id);

            if (Input::hasFile('tempath'))
            {
              $file = Input::file('tempath');
              $tmpFilePath = '/images/upload/';
              $tmpFileName = time().$file->getClientOriginalName();
              $file = $file->move(base_path().$tmpFilePath, $tmpFileName);
              $path = $tmpFilePath . $tmpFileName;
              $data['testimonial_user_image_url'] = $tmpFileName;
              unset($data['tempath']);
            }
            if($query)
            {
              $user = $query;

              foreach($data as $key => $value)
              {
                $user->$key = $value;
              }

              if($user->save())
              {
                $response['status'] = "1";
                $response['data'] = $user;
                $response['message'] = "Updation Successfull";
              }
            }

          } else {
            $response['status'] = "0";
            $response['data'] = array();
            $response['message'] = $validator->errors()->first();
          }
      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
          $response = [ 'data' => [] ];
          $statusCode = 200;
          $query = Testimonial::find($id);

          if($query)
          {
            if($query->delete())
            {
              unlink(base_path().'/images/upload/'.$query->testimonial_user_image_url);
              $response['status'] = "1";
              $response['message'] = "Removed Successfully";
            }
          } else {
            $response['status'] = "0";
            $response['message'] = "No Record Found";
          }

      } catch (Exception $e){
          $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
      } finally{
          return response()->json($response, $statusCode);
      }
    }


}

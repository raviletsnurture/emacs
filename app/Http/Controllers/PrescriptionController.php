<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use App\Prescription;
use App\Medicine;
use Validator;
use App\User;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->json()->get('data');

      if(!empty($data))
      {
        $returnData = array();
        $statusCode = 200;
        $optionalRules = array();

        $rules = array(
              'patient_id' => 'required',
              'doctor_id' => 'required'
        );

        $validator = Validator::make($data, array_merge($rules, $optionalRules));
        if(!$validator->fails())
        {
          if($prescription = Prescription::create($data))
          {
              if($prescription->save())
              {
                  $pId = $prescription->prescription_id;
                  $returnData['status'] = "1";
                  $returnData['message'] = "Medication added successfully.";
                  $returnData['data'] = Prescription::find($prescription->prescription_id);

                  if(isset($data['medicines']))
                  {
                    $medicines = $data['medicines'];
                    foreach($medicines as $medicine)
                    {
                      $medicine['prescription_id'] = $pId;
                      $medicineAdd = Medicine::create($medicine);
                      $medicineAdd->save();
                    }
                    $returnData['data']['medicines'] = Prescription::find($prescription->prescription_id)->medicines;
                  }
                  echo json_encode($returnData);
                  exit();


              }
              else
              {
                $returnData['status'] = "0";
                $returnData['message'] = "Some thing went wrong try after some time";
                echo json_encode($returnData);
                exit();
              }
          }
          else
          {
            $returnData['status'] = "0";
            $returnData['message'] = "Some thing went wrong try after some time";
            echo json_encode($returnData);
            exit();
          }

        }
        else
        {
          $returnData['status'] = "0";
          $returnData['message'] = $validator->errors()->first();
          echo json_encode($returnData);
          exit();
        }

      }
      else
      {
        $returnData['status'] = "0";
        $returnData['message'] = "Missing Parameters";
        echo json_encode($returnData);
        exit();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  patient or doctor id
     * @return \Illuminate\Http\Response
     */
    public function listPrescriptions($id)
    {
        if(User::find($id))
        {
          if(User::find($id)->user_type == "1"){
            $compareString = "doctor_id";
          }
          else{
            $compareString = "patient_id";
          }

          $res = Prescription::where($compareString,'=',$id)->with('medicines')->get();

          if(!empty(Auth::id()))
          {
            $prescriptionData = $res;
            $patients = User::select('user_id','firstname', 'lastname')->where('user_type','=','2')->get();
            return view('pages.doctor.writePrescription',compact('prescriptionData','patients'));
          }
          else
          {
            $returnData = array();
            $returnData['status'] = "1";
            $returnData['message'] = "Success.";
            $returnData['data'] = $res;
            echo json_encode($returnData);exit;
          }

        }
        else
        {
          $returnData['status'] = "0";
          $returnData['message'] = "User not found.";
          echo json_encode($returnData);
          exit();
        }
    }

    public function prescriptionDetails(Request $request){

        $data = $request->json()->get('data');

        $data = Prescription::where('prescription_id',$data['prescription_id'])
        ->leftJoin('em_users','em_users.user_id','=','em_prescription.patient_id')->first();

        $medicines = Prescription::where('prescription_id',$data['prescription_id'])->first()->medicines;

        $returnData = array();
        $returnData['status'] = "1";
        $returnData['message'] = "Success.";
        $returnData['data'] = $data;
        $returnData['data']['medicines'] = $medicines;

        echo json_encode($returnData);exit;
    }




}

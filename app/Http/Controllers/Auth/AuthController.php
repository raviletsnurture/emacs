<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Patient;
use App\Doctor;
use App\Member as Member;
use DiseaseInfo;
use Validator;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Input;
use Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use DB;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

  	public function getLogout()
    {
        $path = '/';
        // if(Auth::check()){
        //     $path = '/'.Auth::user()->getRole();
        // }

        Auth::logout();
	      // return back();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : $path);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required',
            'email' => 'required|email|max:255|unique:em_users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        return User::create([
          'firstname' => $data['firstname'],
          'lastname' => $data['lastname'],
          'username' => $data['username'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        return view('pages.auth.register');
    }

    public function redirectPath()
        {
            if (property_exists($this, 'redirectPath')) {
                return $this->redirectPath;
            }

            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/'.Auth::user()->getRole();
            //return redirect('/'.Auth::user()->getRole());
        }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($request->except('password'));
        }

        if($user = $this->create($request->all())){
          $sKey = substr(md5($user->user_id.$user->created_at),0,8);
          $link = route('verification', ['string' => $sKey, 'email' => $user->email]);  // generate link with key and email.

          Mail::send('emails.verification', ['user' => $user, 'link' => $link], function ($m) use ($user) {
              $m->to($user->email, $user->username)->subject('Emacs Verification Process.');
          });

          $request->session()->put('flash_message','Registration Successfull. Please check email to proceed further.');
          return back();

        } else {
          $request->session()->put('flash_message_error','Registration Failed');
          return back();
        }

        return redirect($this->redirectPath());
    }


	public function postLogin(Request $request)
    {

    	if(!empty($request->json()->get('data')))
    	{
    		$wsData = $request->json()->get('data');

    		if($wsData['_token'] == "ws")
        {
	         $validator = Validator::make($wsData, [
		            'email' => 'required', 'password' => 'required',
		        ]);

		        if($validator->fails())
		        {
		        	//$erroData = json_encode($validator->errors()->all()));
		        	$returnData['status'] = "0";
		        	$returnData['message'] = $validator->errors()->first();
      	       echo json_encode($returnData);
		        	exit();
		        }
		        else
            {
			        if(User::where('email','=',$wsData["email"])->count() > 0 )
		    		  {
    		    			$user = User::where('email','=',$wsData["email"])->first();

                  if(Hash::check($wsData['password'],$user->password) )
                  {
    		    				$returnData['status'] = "1";
    				        $returnData['message'] = "Success";
                    $returnData['data'] = User::find($user->user_id);

                    if($returnData['data']['user_type'] == "1")
                    {
	                    $id = $user->user_id;
                    	$degree = DB::table('em_doctor_degree')
    				              ->select('degree_name','college_name','degree_description','start_from','start_from')
    				              ->where('doctor_id','=',$id)
    				              ->get();
                      $experience = DB::table('em_doctor_experience')
                                ->select('designation','description','currently_working','hospital_name','location','start_date','end_date')
                                ->where('doctor_id','=',$id)
                                ->get();
                      $award = DB::table('em_doctor_award')
                                ->select('award_title','award_description')
                                ->where('doctor_id','=',$id)
                                ->get();
                      $group = DB::table('em_doctor_group')
                                ->select('group_title','group_description')
                                ->where('doctor_id','=',$id)
                                ->get();
                      $research_paper = DB::table('em_doctor_research_paper')
                                ->select('research_paper_title','research_paper_description')
                                ->where('doctor_id','=',$id)
                                ->get();
                      $meeting = DB::table('em_doctor_meeting')
                                ->select('meeting_title','meeting_description')
                                ->where('doctor_id','=',$id)
                                ->get();

                      if(User::find($user->user_id)->doctor)
                      {
                        $returnData['data']['doctor'] = $returnData['data']->doctor;
                      }
                      $returnData['data']['doctor']['degree'] = $degree;
          				    $returnData['data']['doctor']['experience'] = $experience;
          				    $returnData['data']['doctor']['award'] = $award;
          				    $returnData['data']['doctor']['group'] = $group;
          				    $returnData['data']['doctor']['research_paper'] = $research_paper;
          				    $returnData['data']['doctor']['meeting'] = $meeting;

                    }
                    else
                    {
                      $returnData['data']['patient'] = $returnData['data']->patient;
                      if($returnData['data']->patient)
                      {
                          $returnData['data']['patient']['members'] = Patient::find($returnData['data']->patient->patient_id)->members;
                      }
                    }



                    unset($returnData['data']['is_verified']);
                    unset($returnData['data']['is_blocked']);
                    unset($returnData['data']['status']);
                    unset($returnData['data']['created_at']);
                    unset($returnData['data']['updated_at']);
                    if($returnData['data']['user_type'] == "2"){
                        unset($returnData['data']['doctor_speciality']);
                    }

                    if($returnData['data']['user_type'] == "1"){
                        unset($returnData['data']['disease_name']);
                    }
    				        echo json_encode($returnData);

    		    			}
    		    			else
                  {
    		    				  $returnData['status'] = "0";
    				        	$returnData['message'] = "credentials do not match";
    				        	echo json_encode($returnData);
    		    			}
		    		}
		    		else
            {
		    			  $returnData['status'] = "0";
				        $returnData['message'] = "Email not found in our records";
				        echo json_encode($returnData);
		    		}
		        	exit();
		        }
	    	}
    	}


      $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

      $throttles = $this->isUsingThrottlesLoginsTrait();

      if ($throttles && $this->hasTooManyLoginAttempts($request))
      {
        return $this->sendLockoutResponse($request);
      }

      $credentials = $this->getCredentials($request);

      if (Auth::attempt($credentials, $request->has('remember')))
      {
        return $this->handleUserWasAuthenticated($request, $throttles);
      }

      if ($throttles)
      {
            $this->incrementLoginAttempts($request);
      }

      return back()
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }
}

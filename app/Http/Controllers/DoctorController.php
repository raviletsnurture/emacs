<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Validator;
use App\User;
use App\Doctor;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use SpecialityInfo;
use Auth;
use DB;

class DoctorController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    if(isset(Auth::user()->user_id)){
      $userid = Auth::user()->user_id;

      return redirect('doctor/detail/'.$userid);

      //['user.preferences.update', Auth::user()->id]
    }
    else{
      return " Doctor page";
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return "user/create";
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->json()->get('data');

    if(!isset($data['password']) || $data['password'] == "") $data['password'] = $data['username'];

    try{
        $response = [ 'data' => [] ];
        $statusCode = 200;
        $optionalRules = array();

        $rules = array(
            'username' => 'required|max:30',
            'firstname' => 'required|max:30',
            'email' => 'required|email|max:100|unique:em_users,email',
            'user_type' => 'required'
        );

        $validator = Validator::make($data, array_merge($rules, $optionalRules));

        if(!$validator->fails()) {

          if(isset($data['password']) && $data['password'] != "") $data['password'] = Hash::make($data['password']);

          if($user = User::create($data))
          {
              if($user->save()){
                $response['status'] = "1";
                $response['data'] = User::find($user->user_id);
                $response['message'] = "Registration Successfull. !!";
              }
          }

        } else {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = $validator->errors()->first();
        }
    } catch (Exception $e){
        $statusCode = 400;  // Bad Request Error Code
          $response['status'] = 0;
          $response['message'] = "Bad Request Error";
    } finally{
        return response()->json($response, $statusCode);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

    $specialityInfo = new SpecialityInfo;
    $speciality = $specialityInfo->getall();

    $doctorDetails = $this->getProfileDetail($id);
    $experience = $this->getExperienceDetail($id);
    $degree = $this->getDegreeDetail($id);
    $award = $this->getAwardDetail($id);
    $group = $this->getGroupDetail($id);
    $research_paper = $this->getResearchDetail($id);
    $meeting = $this->getMeetingDetail($id);
    //  echo "<pre>";
    //  print_r($group);exit;
    // dd($doctorDetails['data']);
    //  foreach($doctorDetails['data']->doctor as $deg){
    //    echo "<pre>";
    //    print_r($deg);
    //  }exit;

    try{
        $response = [ 'data' => [] ];
        $statusCode = 200;
        $query = User::find($id);

        if($query)
        {
          $user = $query->toArray();  // get array of attributes->values from User Obj
          $response['data'] = $user;  // Set Response Obj
          $response['speciality'] = $speciality; //gets speciality array from db
          $response['status'] = 1;
        } else {
          $response['message'] = "Requested User Not Found";  // Set Response Obj
          $response['status'] = 0;
        }

    } catch (Exception $e){
        $statusCode = 400;  // Bad Request Error Code
        $response['status'] = 0;
        $response['message'] = "Bad Request Error";
        return response()->json($response, $statusCode);
    } finally{
        //return response()->json($response, $statusCode);
        return view('pages.doctor.dashboard',compact('doctorDetails','experience','degree','award','group','research_paper','meeting'));
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->json()->get('data');

    try{
        $response = [ 'data' => [] ];
        $statusCode = 200;

        $validator = Validator::make($data, [
          'username' => 'required|max:30',
          'firstname' => 'required|max:30',
          'email' => 'required|email|max:100|unique:em_users,email,'.$id.',user_id',
          'user_type' => 'required'
        ]);

        if(!$validator->fails()) {
          $query = User::find($id);

          //print_r($query->count()); exit;
          if($query)
          {
            $user = $query;

            foreach($data as $key => $value)
            {
              $user->$key = $value;
            }

            if($user->save())
            {
              $response['status'] = "1";
              $response['data'] = $user;
              $response['message'] = "Updation Successfull";
            }
          }

        } else {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = $validator->errors()->first();
        }
    } catch (Exception $e){
        $statusCode = 400;  // Bad Request Error Code
        $response['status'] = 0;
        $response['message'] = "Bad Request Error";
    } finally{
        return response()->json($response, $statusCode);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try{
        $response = [ 'data' => [] ];
        $statusCode = 200;
        $query = User::find($id);

        if($query)
        {
          if($query->delete())
          {
            $response['status'] = "1";
            $response['message'] = "Removed Successfull";
          }
        } else {
          $response['status'] = "0";
          $response['message'] = "No User Found";
        }

    } catch (Exception $e){
        $statusCode = 400;  // Bad Request Error Code
        $response['status'] = 0;
        $response['message'] = "Bad Request Error";
    } finally{
        return response()->json($response, $statusCode);
    }
  }


  /**
   * get doctor profile details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getProfileDetail($id)
  {

      if(User::find($id))
      {
        $user = User::find($id);
        $pData = $user->doctor;
        $response['status'] = "1";
        $response['data'] = $user;
        $response['data']['doctor'] = $pData;
        $response['message'] = "Success.";
        if(isset(Auth::user()->user_id)){
          return $response;
        }else{
          echo json_encode($response);exit;
        }
      }
      else
      {
        $response['status'] = "0";
        $response['message'] = "User not found.";
        echo json_encode($response);exit;
      }

  }

  /**
   * get doctor degree details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getDegreeDetail($id)
  {
    if(User::find($id))
    {
      $result = DB::table('em_doctor_degree')
              ->select('degree_name','college_name','degree_description','end_to','start_from')
              ->where('doctor_id','=',$id)
              ->get();

      $response['status'] = "1";
      $response['data'] = $result;
      $response['message'] = "Success.";
      if(isset(Auth::user()->user_id)){
        return $response;
      }else {
        echo json_encode($response);exit;
      }
    }
    else
    {
      $response['status'] = "0";
      $response['message'] = "User not found.";
      echo json_encode($response);exit;
    }
  }


  /**
   * get doctor experience details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getExperienceDetail($id)
  {
    if(User::find($id))
    {
      $result = DB::table('em_doctor_experience')
              ->select('designation','description','currently_working','hospital_name','location','start_date','end_date')
              ->where('doctor_id','=',$id)
              ->get();

      $response['status'] = "1";
      $response['data'] = $result;
      $response['message'] = "Success.";
      if(isset(Auth::user()->user_id)){
        return $response;
      }else {
        echo json_encode($response);exit;
      }
    }
    else
    {
      $response['status'] = "0";
      $response['message'] = "User not found.";
      echo json_encode($response);exit;
    }
  }


  /**
   * get doctor award details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getAwardDetail($id)
  {
    if(User::find($id))
    {
      $result = DB::table('em_doctor_award')
              ->select('award_title','award_description')
              ->where('doctor_id','=',$id)
              ->get();

      $response['status'] = "1";
      $response['data'] = $result;
      $response['message'] = "Success.";
      if(isset(Auth::user()->user_id)){
        return $response;
      }else {
        echo json_encode($response);exit;
      }
    }
    else
    {
      $response['status'] = "0";
      $response['message'] = "User not found.";
      echo json_encode($response);exit;
    }
  }


  /**
   * get doctor group details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getGroupDetail($id)
  {
    if(User::find($id))
    {
      $result = DB::table('em_doctor_group')
              ->select('group_title','group_description')
              ->where('doctor_id','=',$id)
              ->get();

      $response['status'] = "1";
      $response['data'] = $result;
      $response['message'] = "Success.";
      if(isset(Auth::user()->user_id)){
        return $response;
      }else {
        echo json_encode($response);exit;
      }
    }
    else
    {
      $response['status'] = "0";
      $response['message'] = "User not found.";
      echo json_encode($response);exit;
    }
  }


  /**
   * get doctor research details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getResearchDetail($id)
  {
    if(User::find($id))
    {
      $result = DB::table('em_doctor_research_paper')
              ->select('research_paper_title','research_paper_description')
              ->where('doctor_id','=',$id)
              ->get();

      $response['status'] = "1";
      $response['data'] = $result;
      $response['message'] = "Success.";
      if(isset(Auth::user()->user_id)){
        return $response;
      }else {
        echo json_encode($response);exit;
      }
    }
    else
    {
      $response['status'] = "0";
      $response['message'] = "User not found.";
      echo json_encode($response);exit;
    }
  }


  /**
   * get doctor meeting details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function getMeetingDetail($id)
  {
    if(User::find($id))
    {
      $result = DB::table('em_doctor_meeting')
              ->select('meeting_title','meeting_description')
              ->where('doctor_id','=',$id)
              ->get();

      $response['status'] = "1";
      $response['data'] = $result;
      $response['message'] = "Success.";
      if(isset(Auth::user()->user_id)){
        return $response;
      }else {
        echo json_encode($response);exit;
      }
    }
    else
    {
      $response['status'] = "0";
      $response['message'] = "User not found.";
      echo json_encode($response);exit;
    }
  }


  /**
   * add doctor profile details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addProfileDetail(Request $request)
  {

    $data = $request->json()->get('data');

    if(isset($data) && $data['_token'] == "ws")
    {
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'firstname' => 'required|max:30',
          'email' => 'required|email|max:100|unique:em_users,email',
          'user_type' => 'required'
      );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails()) {

        if(isset($data['password']) && $data['password'] != "") $data['password'] = Hash::make($data['password']);

        if($user = User::create($data))
        {

            if($user->save()){
              $data['user_id'] = $user->user_id;
              if($doctor = Doctor::create($data))
              {
                  if($doctor->save())
                  {
                    $response['status'] = "1";
                    $response['data'] = User::find($user->user_id);
                    $response['message'] = "Doctor added successfully. !!";

                    $response['data']['doctor'] = User::find("$user->user_id")->doctor;

                    unset($response['data']['created_at']);
                    unset($response['data']['updated_at']);

                    echo json_encode($response);exit;
                  }
              }

            }
        }

      } else {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
        $response['status'] = "0";
        $response['message'] = "Parameter missing.";
        echo json_encode($response);exit;
    }//code ends here.


  }

  /**
   * add doctor degree details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addDegreeDetail(Request $request)
  {

    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'doctor_id' => 'required',
          'degree_name' => 'required|max:100',
          'college_name' => 'required|max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $insert = DB::table('em_doctor_degree')->insert($data);
        if($insert)
        {

          $response['status'] = "1";
          $response['message'] = "Degree added successfully. !!";
          $response['data'] = array();

          echo json_encode($response);exit;
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "Error : Can not add details. Try after some time";
          echo json_encode($response);exit;
        }

      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }

  }


  /**
   * add doctor experience details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addExperienceDetail(Request $request)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'doctor_id' => 'required',
          'designation' => 'required|max:100',
          'hospital_name' => 'required|max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $insert = DB::table('em_doctor_experience')->insert($data);
        if($insert)
        {

          $response['status'] = "1";
          $response['message'] = "Experience added successfully. !!";
          $response['data'] = array();

          echo json_encode($response);exit;
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "Error : Can not add details. Try after some time";
          echo json_encode($response);exit;
        }

      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * add doctor award details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addAwardDetail(Request $request)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'doctor_id' => 'required',
          'award_title' => 'required|max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $insert = DB::table('em_doctor_award')->insert($data);
        if($insert)
        {

          $response['status'] = "1";
          $response['message'] = "Award added successfully. !!";
          $response['data'] = array();

          echo json_encode($response);exit;
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "Error : Can not add details. Try after some time";
          echo json_encode($response);exit;
        }

      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * add doctor group details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addGroupDetail(Request $request)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'doctor_id' => 'required',
          'group_title' => 'required|max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $insert = DB::table('em_doctor_group')->insert($data);
        if($insert)
        {

          $response['status'] = "1";
          $response['message'] = "Group added successfully. !!";
          $response['data'] = array();

          echo json_encode($response);exit;
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "Error : Can not add details. Try after some time";
          echo json_encode($response);exit;
        }

      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * add doctor research details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addResearchDetail(Request $request)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'doctor_id' => 'required',
          'research_paper_title' => 'required|max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $insert = DB::table('em_doctor_research_paper')->insert($data);
        if($insert)
        {

          $response['status'] = "1";
          $response['message'] = "Research Paper added successfully. !!";
          $response['data'] = array();

          echo json_encode($response);exit;
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "Error : Can not add details. Try after some time";
          echo json_encode($response);exit;
        }

      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * add doctor meeting details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function addMeetingDetail(Request $request)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'doctor_id' => 'required',
          'meeting_title' => 'required|max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $insert = DB::table('em_doctor_meeting')->insert($data);
        if($insert)
        {

          $response['status'] = "1";
          $response['message'] = "Meeting added successfully. !!";
          $response['data'] = array();

          echo json_encode($response);exit;
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "Error : Can not add details. Try after some time";
          echo json_encode($response);exit;
        }

      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }



  /**
   * edit doctor profile details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editProfileDetail(Request $request, $id)
  {

    if($request->json()->get('data') != "")
    {
        $data = $request->json()->get('data');
        $response = [ 'data' => [] ];
        $statusCode = 200;
        $optionalRules = array();

        $rules = array(
            'firstname' => 'max:30',
            'email' => 'required|email|max:100|unique:em_users,email,'.$id.',user_id'
        );

        $validator = Validator::make($data, array_merge($rules, $optionalRules));

        if(!$validator->fails())
        {
          $query = User::find($id);

          if($query)
          {
            $user = $query;
            if($query->update($data, $id))
            {

              unset($data['user_id']);
              $doctorQuery = Doctor::where('user_id', '=', $id)->first();
              $dId = $doctorQuery['doctor_id'];

              if($doctorQuery->update($data, $dId))
              {
                $response['status'] = "1";
                $response['data'] = $user;
                $response['data']['doctor'] = $query->doctor;
                $response['message'] = "Updation Successfull";
                echo json_encode($response);
                exit;
              }
              else{
                $response['status'] = "1";
                $response['data'] = $user;
                $response['message'] = "Updation Successfull";
                echo json_encode($response);exit;
              }
            }
          }

        } else {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = $validator->errors()->first();
          echo json_encode($response);exit;
        }

    }
    else
    {
        $response['status'] = "0";
        $response['message'] = "Parameter missing.";
        echo json_encode($response);exit;
    }//code ends here.
  }

  /**
   * edit doctor degree details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editDegreeDetail(Request $request, $id)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $dId = $data['degree_id'];

      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'degree_name' => 'max:100',
          'college_name' => 'max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $query = DB::table('em_doctor_degree')
                ->where('doctor_id','=',$id)
                ->where('degree_id','=',$dId)
                ->get();
        if($query)
        {
          $update = DB::table('em_doctor_degree')
                  ->where('doctor_id','=',$id)
                  ->where('degree_id','=',$dId)
                  ->update($data);
          if($update)
          {
            $updatedData = DB::table('em_doctor_degree')
                    ->where('doctor_id','=',$id)
                    ->where('degree_id','=',$dId)
                    ->first();
            unset($updatedData->created_at);
            unset($updatedData->updated_at);

            $response['status'] = "1";
            $response['message'] = "Updation Successfull";
            $response['data'] = $updatedData;


            echo json_encode($response);exit;
          }
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "User and/or degree does not exists.";
          echo json_encode($response);exit;
        }


      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * edit doctor experience details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editExperienceDetail(Request $request, $id)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $dId = $data['experience_id'];

      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'designation' => 'max:100',
          'hospital_name' => 'max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $query = DB::table('em_doctor_experience')
                ->where('doctor_id','=',$id)
                ->where('experience_id','=',$dId)
                ->get();
        if($query)
        {
          $update = DB::table('em_doctor_experience')
                  ->where('doctor_id','=',$id)
                  ->where('experience_id','=',$dId)
                  ->update($data);
          if($update)
          {
            $updatedData = DB::table('em_doctor_experience')
                    ->where('doctor_id','=',$id)
                    ->where('experience_id','=',$dId)
                    ->first();
            unset($updatedData->created_at);
            unset($updatedData->updated_at);

            $response['status'] = "1";
            $response['message'] = "Updation Successfull";
            $response['data'] = $updatedData;


            echo json_encode($response);exit;
          }
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "User and/or experience does not exists.";
          echo json_encode($response);exit;
        }


      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * edit doctor award details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editAwardDetail(Request $request, $id)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $dId = $data['award_id'];

      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'award_title' => 'max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $query = DB::table('em_doctor_award')
                ->where('doctor_id','=',$id)
                ->where('award_id','=',$dId)
                ->get();
        if($query)
        {
          $update = DB::table('em_doctor_award')
                  ->where('doctor_id','=',$id)
                  ->where('award_id','=',$dId)
                  ->update($data);
          if($update)
          {
            $updatedData = DB::table('em_doctor_award')
                    ->where('doctor_id','=',$id)
                    ->where('award_id','=',$dId)
                    ->first();
            unset($updatedData->created_at);
            unset($updatedData->updated_at);

            $response['status'] = "1";
            $response['message'] = "Updation Successfull";
            $response['data'] = $updatedData;


            echo json_encode($response);exit;
          }
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "User and/or award does not exists.";
          echo json_encode($response);exit;
        }


      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * edit doctor group details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editGroupDetail(Request $request, $id)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $dId = $data['group_id'];

      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'group_title' => 'max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $query = DB::table('em_doctor_group')
                ->where('doctor_id','=',$id)
                ->where('group_id','=',$dId)
                ->get();
        if($query)
        {
          $update = DB::table('em_doctor_group')
                  ->where('doctor_id','=',$id)
                  ->where('group_id','=',$dId)
                  ->update($data);
          if($update)
          {
            $updatedData = DB::table('em_doctor_group')
                    ->where('doctor_id','=',$id)
                    ->where('group_id','=',$dId)
                    ->first();
            unset($updatedData->created_at);
            unset($updatedData->updated_at);

            $response['status'] = "1";
            $response['message'] = "Updation Successfull";
            $response['data'] = $updatedData;


            echo json_encode($response);exit;
          }
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "User and/or group does not exists.";
          echo json_encode($response);exit;
        }


      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * edit doctor research details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editResearchDetail(Request $request, $id)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $dId = $data['research_paper_id'];

      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'group_title' => 'max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $query = DB::table('em_doctor_research_paper')
                ->where('doctor_id','=',$id)
                ->where('research_paper_id','=',$dId)
                ->get();
        if($query)
        {
          $update = DB::table('em_doctor_research_paper')
                  ->where('doctor_id','=',$id)
                  ->where('research_paper_id','=',$dId)
                  ->update($data);
          if($update)
          {
            $updatedData = DB::table('em_doctor_research_paper')
                    ->where('doctor_id','=',$id)
                    ->where('research_paper_id','=',$dId)
                    ->first();
            unset($updatedData->created_at);
            unset($updatedData->updated_at);

            $response['status'] = "1";
            $response['message'] = "Updation Successfull";
            $response['data'] = $updatedData;


            echo json_encode($response);exit;
          }
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "User and/or research paper does not exists.";
          echo json_encode($response);exit;
        }


      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }


  /**
   * edit doctor meeting details
   @by Nilesh@letsnurture
    @on 8th Feb 2016
    *
    */
  public function editMeetingDetail(Request $request, $id)
  {
    if($request->json()->get('data') != "")
    {
      $data = $request->json()->get('data');
      $dId = $data['meeting_id'];

      $response = [ 'data' => [] ];
      $statusCode = 200;
      $optionalRules = array();

      $rules = array(
          'meeting_title' => 'max:100'
          );

      $validator = Validator::make($data, array_merge($rules, $optionalRules));

      if(!$validator->fails())
      {
        $query = DB::table('em_doctor_meeting')
                ->where('doctor_id','=',$id)
                ->where('meeting_id','=',$dId)
                ->get();
        if($query)
        {
          $update = DB::table('em_doctor_meeting')
                  ->where('doctor_id','=',$id)
                  ->where('meeting_id','=',$dId)
                  ->update($data);
          if($update)
          {
            $updatedData = DB::table('em_doctor_meeting')
                    ->where('doctor_id','=',$id)
                    ->where('meeting_id','=',$dId)
                    ->first();
            unset($updatedData->created_at);
            unset($updatedData->updated_at);

            $response['status'] = "1";
            $response['message'] = "Updation Successfull";
            $response['data'] = $updatedData;


            echo json_encode($response);exit;
          }
        }
        else
        {
          $response['status'] = "0";
          $response['data'] = array();
          $response['message'] = "User and/or meeting does not exists.";
          echo json_encode($response);exit;
        }


      }
      else
      {
        $response['status'] = "0";
        $response['data'] = array();
        $response['message'] = $validator->errors()->first();
        echo json_encode($response);exit;
      }

    }
    else
    {
      $response['status'] = "0";
      $response['data'] = array();
      $response['message'] = "Parameter missing.";
      echo json_encode($response);exit;
    }
  }



}

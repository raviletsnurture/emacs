<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medication;
use Validator;
use Auth;

class MedicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $data = $request->json()->get('data');

      if(!empty($data))
      {
        $returnData = array();
        $statusCode = 200;
        $optionalRules = array();

        $rules = array(
              'user_id' => 'required',
              'disease_name' => 'required|max:200',
              'dosage' => 'required|max:200',
              'taking_since' => 'required'
        );

        $validator = Validator::make($data, array_merge($rules, $optionalRules));
        if(!$validator->fails())
        {
          if($medication = Medication::create($data))
          {
              if($medication->save())
              {
                if(isset(Auth::user()->user_id)){
                  $userid = Auth::user()->user_id;
                  return redirect('doctor/detail/'.$userid);

                  //['user.preferences.update', Auth::user()->id]
                }
                else
                {
                  $returnData['status'] = "1";
                  $returnData['message'] = "Medication added successfully.";
                  $returnData['data'] = Medication::find($medication->medicine_id);
                  echo json_encode($returnData);
                  exit();
                }

              }
              else
              {
                $returnData['status'] = "0";
                $returnData['message'] = "Some thing went wrong try after some time";
                echo json_encode($returnData);
                exit();
              }
          }
          else
          {
            $returnData['status'] = "0";
            $returnData['message'] = "Some thing went wrong try after some time";
            echo json_encode($returnData);
            exit();
          }

        }
        else
        {
          $returnData['status'] = "0";
          $returnData['message'] = $validator->errors()->first();
          echo json_encode($returnData);
          exit();
        }

      }
      else
      {
        $returnData['status'] = "0";
        $returnData['message'] = "Missing Parameters";
        echo json_encode($returnData);
        exit();
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Lists medication hiostory of particular patient
     *
     * @param  int  $userID
     * @return array containing patient previous medication
     */
    public function medicationList($userID)
    {
      
      if(Medication::where('user_id','=',$userID)->count() > 0)
      {
        $list = Medication::where('user_id','=',$userID)->get();
        $returnData['status'] = "1";
        $returnData['message'] = "Success.";
        $returnData['data'] = $list;
        echo json_encode($returnData);
        exit();
      }
      else
      {
        $returnData['status'] = "0";
        $returnData['message'] = "No medication found for this patient.";
        echo json_encode($returnData);
        exit();
      }

    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::get('/', function () {
    return view('pages.home');
});

Route::get('chat', function () {
    return view('chat');
});

Route::get('signin', function () {
    return view('pages.signin');
});

Route::post('/sendResetPassLink', 'PatientController@sendResetPAssLink');
Route::post('/resetpass', 'PatientController@setNewPass');
Route::get('/resetpass/{id}', 'PatientController@changePass');

Route::get('signup','PatientController@registerPatient' );
Route::post('isEmailExist','PatientController@isEmailExist');

/* --------------------------------- Authentication Requests Handling ------------------------------- */
Route::get('auth/admin/login', function(){
  return view('pages.admin.auth.login');
});
Route::get('auth/patient/login', function(){
  return view('pages.auth.login');
});
Route::get('auth/doctor/login', function(){
  return view('pages.auth.login');
});
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/SignUp', 'Auth\AuthController@getRegister');
Route::post('auth/SignUp', 'Auth\AuthController@postRegister');
Route::get('verify/{string}/{email}',['as' => 'verification', 'uses' => 'PatientController@verify']);

Route::group(['middleware' => 'auth'], function()
{
  /* --------------------------------- Requests Group Handling ------------------------------- */
    Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function() {

    Route::resource('/', 'AdminController');
    Route::get('/patients', 'AdminController@index');
    Route::resource('/patients', 'PatientController', array('only' => array('store', 'show', 'update', 'destroy')));
    Route::get('/doctors', 'AdminController@doctorsList');

    Route::resource('/doctors', 'DoctorController', array('only' => array('store', 'show', 'update', 'destroy')));

    Route::get('/doctor/speciality', 'doctorSpecialityController@doctorsTypeList');
    Route::resource('/doctors/speciality', 'doctorSpecialityController');

    Route::get('/pharmacy', 'PharmacyController@index');
    Route::resource('/pharmacy', 'PharmacyController');

    Route::get('/disease', 'DiseaseController@index');
    Route::resource('/disease', 'DiseaseController');

    Route::get('/package', 'PackageController@index');
    Route::resource('/package', 'PackageController');

    Route::get('/package', 'PackageController@index');
    Route::resource('/package', 'PackageController');

    Route::get('/testimonial', 'TestimonialController@index');
    Route::post('/testimonial/update/{id}', 'TestimonialController@update');
    Route::resource('/testimonial', 'TestimonialController');

    Route::get('/page', 'PageController@index');
    Route::resource('/page', 'PageController');


  });

  /* --------------------------------- Requests Group Handling ------------------------------- */
  Route::group(['prefix' => 'patient', 'middleware' => ['role:patient']], function() {

    Route::get('/medicalInquiry', 'QueryController@queryList');
    Route::post('/details','QueryController@queryDetails');

    Route::get('/detail/{id}', 'PatientController@show');
    Route::resource('/', 'PatientController');

  });

  /* --------------------------------- Requests Group Handling ------------------------------- */
  Route::group(['prefix' => 'doctor', 'middleware' => ['role:doctor']], function() {

    Route::post('/details','QueryController@queryDetails');
    Route::get('/medicalInquiry', 'QueryController@queryList');

  	Route::get('/detail/{id}', 'DoctorController@show');
    Route::resource('/', 'DoctorController');

    Route::get('/prescription/{id}', 'PrescriptionController@listPrescriptions');
  });

});

Route::group(['prefix' => 'api'], function()
{

  Route::get('providers/list', 'PatientController@providersList');
  Route::get('disease/list', 'DiseaseController@diseaseList');
  Route::resource('disease', 'DiseaseController');

  Route::get('/country/list', 'PatientController@countryList');
  Route::get('patient/detail/{id}', 'PatientController@show');
  Route::post('patient/register','PatientController@addPatient');

  Route::post('patient/contactDetails','PatientController@getContactDetails');
  Route::post('patient/loginDetails','PatientController@getLoginDetails');
  Route::post('patient/insuranceDetails','PatientController@getInsuranceDetails');
  Route::post('patient/paymentMethod','PatientController@getPaymentMethod');

  Route::resource('patient', 'PatientController') ;

  Route::get('doctor/profile_detail/{id}', 'DoctorController@getProfileDetail');
  Route::get('doctor/degree_detail/{id}', 'DoctorController@getDegreeDetail');
  Route::get('doctor/experience_detail/{id}', 'DoctorController@getExperienceDetail');
  Route::get('doctor/award_detail/{id}', 'DoctorController@getAwardDetail');
  Route::get('doctor/group_detail/{id}', 'DoctorController@getGroupDetail');
  Route::get('doctor/research_detail/{id}', 'DoctorController@getResearchDetail');
  Route::get('doctor/meeting_detail/{id}', 'DoctorController@getMeetingDetail');

  Route::post('doctor/profile_detail', 'DoctorController@addProfileDetail');
  Route::post('doctor/degree_detail', 'DoctorController@addDegreeDetail');
  Route::post('doctor/experience_detail', 'DoctorController@addExperienceDetail');
  Route::post('doctor/award_detail', 'DoctorController@addAwardDetail');
  Route::post('doctor/group_detail', 'DoctorController@addGroupDetail');
  Route::post('doctor/research_detail', 'DoctorController@addResearchDetail');
  Route::post('doctor/meeting_detail', 'DoctorController@addMeetingDetail');

  Route::put('doctor/profile_detail/{id}', 'DoctorController@editProfileDetail');
  Route::put('doctor/degree_detail/{id}', 'DoctorController@editDegreeDetail');
  Route::put('doctor/experience_detail/{id}', 'DoctorController@editExperienceDetail');
  Route::put('doctor/award_detail/{id}', 'DoctorController@editAwardDetail');
  Route::put('doctor/group_detail/{id}', 'DoctorController@editGroupDetail');
  Route::put('doctor/research_detail/{id}', 'DoctorController@editResearchDetail');
  Route::put('doctor/meeting_detail/{id}', 'DoctorController@editMeetingDetail');


  Route::resource('doctor', 'DoctorController') ;

  Route::get('medication/list/{id}', 'MedicationController@medicationList') ;
  Route::resource('medication', 'MedicationController') ;

  Route::post('/prescription/details','PrescriptionController@prescriptionDetails');
  Route::get('prescription/list/{id}', 'PrescriptionController@listPrescriptions') ;
  Route::resource('prescription', 'PrescriptionController') ;
});

Route::get('api/query', 'QueryController@index');
Route::post('api/query/list', 'QueryController@queryList');
Route::resource('api/query', 'QueryController');

Route::get('api/member', 'MemberController@index');
Route::post('api/member/list', 'PatientController@memberList');
Route::resource('api/member', 'MemberController');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  protected $table = 'em_cms';
   protected $primaryKey = 'page_id';


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */

  protected $fillable = ['page_title','page_content'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
}

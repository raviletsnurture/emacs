<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
     protected $table = 'em_users';
     protected $primaryKey = 'user_id';
     //protected static $_allowUnwatedUserData = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname','lastname','email', 'password','user_type','gender','address_line1','address_line2','city','country','zip','credit_card_number','expiry_month','expiry_year'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getRole() // 0 = Admin || 1 = Doctor || 2 = user
    {
      switch($this->user_type) {
        case 0:
            return "admin"; break;
        case 1:
            return "doctor"; break;
        default:
            return "patient"; break;
      }
    }

	/*public function newQuery()  // method overriding for restrict unverified users to allow in any.
    {
         $query = parent::newQuery();

         if(! static::$_allowUnwatedUserData)
         {
             $query->where('is_verified', '=', 1);
         }
         else{
             static::$_allowUnwatedUserData = false;
         }

         if(! static::$_adminUser)
         {
             $query->where('user_type', '!=', 'sAdmin');
         }
         else{
             static::$_adminUser = false;
         }

         return $query;
    }*/

    public function hasRole($role) // 0 = Admin || 1 = Doctor || 2 = user
    {
      return ($this->user_type == 0 && $role == "admin" ||
       $this->user_type == 1 && $role == "doctor" ||
       $this->user_type == 2 && $role == "patient");
    }

    public function patient() // 0 = Admin || 1 = Doctor || 2 = user
    {
      return $this->hasOne('App\Patient','user_id','user_id');
    }

    public function doctor() // 0 = Admin || 1 = Doctor || 2 = user
    {
      return $this->hasOne('App\Doctor', 'user_id', 'user_id');
    }

    // public function getUserTypeAttribute($user_type)
    // {
    //   switch($user_type) {
    //     case 0:
    //         return "admin"; break;
    //     case 1:
    //         return "doctor"; break;
    //     default:
    //         return "user"; break;
    //   }
    // }
}

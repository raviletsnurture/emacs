<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

     protected $table = 'em_packages';
     protected $primaryKey = 'package_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [];

	 protected $fillable = ['package_price','package_duration','package_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
    protected $table = "em_pharmacy";
    protected  $primaryKey = "pharmacy_id";  
    
    protected $fillable = ['pharmacy_name','pharmacy_addr1','pharmacy_addr2','pharmacy_city','pharmacy_state','pharmacy_country','pharmacy_zip','pharmacy_lat','pharmacy_long'];
    protected $hidden = [];
    
}

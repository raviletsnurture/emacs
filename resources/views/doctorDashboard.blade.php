@extends('layouts.loginedMaster')
@section('content')
<!--main content-->
<div class="" id="wrapper">
  <div class="content">
    <div class="docter_profile_matter_holder">
      <div class="admin_tittle_name">
        <h3>MY PROFILE</h3>
      </div>
      <div class="doctor_solo_dv">
        <div class="img_dv"><img src="{{ URL::asset('images/dotor_profile_image_big.png') }}"  class="img_responsive_max" alt=""/></div>
        <div class="txt_Dv">
          <h1>Dr. Eugene Smith <i class="fa fa-pencil"></i></h1>
          <h2>M.B.B.S, M.D</h2>
          <h4>New York, United States</h4>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="profile_boxes_row row">
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Degrees</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            <div class="incresing_row">
              <h1>M.B.B.</h1>
              <h4> Stanford University School of Medicine, San Francisco</h4>
              <p>Passing Year: 2009</p>
            </div>
            <div class="incresing_row">
              <h1>M.D</h1>
              <h4> Western University of Health Sciences, CA</h4>
              <p>Passing Year:2011</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Experience</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            <div class="incresing_row">
              <h1>M.B.B.s</h1>
              <h4> Stanford University School of Medicine, San Francisco</h4>
              <p>Passing Year: 2009</p>
            </div>
            <div class="incresing_row">
              <h1>M.D</h1>
              <h4> Western University of Health Sciences, CA</h4>
              <p>Passing Year:2011</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Awards</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            <div class="incresing_row">
              <h1>Lorem Ipsum</h1>
              <h4> Lorem Ipsum is simply dummy text of the printing and typesetting industry</h4>
              <p>Passing Year: 2009</p>
            </div>
            <div class="incresing_row">
              <h1>Lorem Ipsum</h1>
              <h4> Western University of Health Sciences, CA</h4>
              <p>Passing Year:2011</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Degrees</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            <div class="incresing_row">
              <h1>M.B.B.</h1>
              <h4> Stanford University School of Medicine, San Francisco</h4>
              <p>Passing Year: 2009</p>
            </div>
            <div class="incresing_row">
              <h1>M.D</h1>
              <h4> Western University of Health Sciences, CA</h4>
              <p>Passing Year:2011</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Experience</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            <div class="incresing_row">
              <h1>M.B.B.s</h1>
              <h4> Stanford University School of Medicine, San Francisco</h4>
              <p>Passing Year: 2009</p>
            </div>
            <div class="incresing_row">
              <h1>M.D</h1>
              <h4> Western University of Health Sciences, CA</h4>
              <p>Passing Year:2011</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Awards</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            <div class="incresing_row">
              <h1>Lorem Ipsum</h1>
              <h4> Lorem Ipsum is simply dummy text of the printing and typesetting industry</h4>
              <p>Passing Year: 2009</p>
            </div>
            <div class="incresing_row">
              <h1>Lorem Ipsum</h1>
              <h4> Western University of Health Sciences, CA</h4>
              <p>Passing Year:2011</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

<!DOCTYPE html>
<html>
    <body>
      <h2>Emacs Verification Instructions</h2><br><br>
      <span><b>Username:</b> {{ $user->username }} </span><br><br>
      <span><b>Email:</b> {{ $user->email }} </span><br><br>
      <span><b>Registered At:</b> {{ $user->created_at }} </span><br><br>
      <span><b>Verification Link:</b> {{ $link }} </span><br><br>
    </body>
</html>

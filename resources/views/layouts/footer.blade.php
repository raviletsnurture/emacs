<!--start_footer_section -->
<footer class="footer_bg">
  <div class="container">
    <div class="footer_links_dv">
    	<div class="footer_links_row">
    		<div class="col-sm-2 Flinks_column ">
    			<h2>COMPANY</h2>
    			<ul>
    				<li><a href="#" class="hvr-grow">Our Mission</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Our Team</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Privacy Policy</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Terms of Use</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Careers</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Contact Us</a>
    				</li>
    			</ul>
    		</div>
    		<div class="col-sm-2 Flinks_column ">
    			<h2>LEARN MORE</h2>
    			<ul>
    				<li><a href="#" class="hvr-grow">Our Blog</a>
    				</li>
    				<li><a href="#" class="hvr-grow">FAQ</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Press</a>
    				</li>
    				<li><a href="#" class="hvr-grow">TV</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Media Inquiries</a>
    				</li>
    			</ul>
    		</div>
    		<div class="col-sm-2 Flinks_column ">
    			<h2>PROVIDERS</h2>
    			<ul>
    				<li><a href="#" class="hvr-grow">Physicians</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Pediatricians</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Psychologists</a>
    				</li>
    				<li><a href="#" class="hvr-grow">Lactation Consultants</a>
    				</li>
    			</ul>
    		</div>
    		<div class="col-sm-6 ">
    			<div class="newftr_img_dv">
    				<img src="{{ URL::asset('images/ftr_dctr.png') }}" class="img_responsive_max" alt="" />
    			</div>
    			<div class="ftr_appstore_btn">
    				<a href="#">
              <img src="{{ URL::asset('images/appstore.png') }}" class="img_responsive_max" alt="Available on app store" />
    				</a>
            <a href="#">
              <img src="{{ URL::asset('images/google_play.png') }}" class="img_responsive_max" alt="Available on google play" />
    				</a>
    			</div>
    		</div>
    	</div>
    </div>
  </div>
  <div class="copyright_bg">
  <a href="#top" class="scrol_top hvr-wobble-vertical"><img	src="{{ URL::asset('images/scroll_top.png') }}" class="img_responsive_max" alt="" />
  </a>

    <div class="container">
    	<div class="ftr_links_dv">
    		<p>Copyright © 2015 EMACS.</p>
    		<div class="follow_us_dv">
    			<a class="hvr-grow" href="#" title="Follow us on Facebook"> <i class="fa fa-facebook"></i>
    			</a> <a class="hvr-grow" href="#" title="Follow us on Twitter"> <i class="fa fa-twitter"></i>
    			</a> <a class="hvr-grow" href="#" title="Follow us on Linkedin"> <i	class="fa fa-linkedin"></i>
    			</a>
    		</div>
    	</div>
    </div>

  </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>

<script src="{{ URL::asset('js/bootstrap-front.js') }}"></script><!--bootstrap-->
<script src="{{ URL::asset('js/jqueryflex_slider.js') }}"></script>
<!-- kwicks slider-->
<script src="{{ URL::asset('js/jquery.kwicks.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-timepicker.js') }}"></script>


<script type="text/javascript">
var Main = Main || {};

jQuery(window).load(function() {
	window.responsiveFlag = jQuery('#responsiveFlag').css('display');
	Main.gallery = new Gallery();

	jQuery(window).resize(function() {
		Main.gallery.update();
	});
});

function Gallery(){
	var self = this,
		container = jQuery('.flexslider'),
		clone = container.clone( false );

	this.init = function (){
		if( responsiveFlag == 'block' ){
			var slides = container.find('.slides');

			slides.kwicks({
				max : 850,
				spacing : 0
			}).find('li > a').click(function (){
				return false;
			});
		} else {
			container.flexslider();
		}
	}
	this.update = function () {
		var currentState = jQuery('#responsiveFlag').css('display');

		if(responsiveFlag != currentState) {

			responsiveFlag = currentState;
			container.replaceWith(clone);
			container = clone;
			clone = container.clone( false );

			this.init();
		}
	}

	this.init();
}
</script>
<script src="{{ URL::asset('js/wow.js') }}"></script><!-- animation-->
<script src="{{ URL::asset('js/jquery.bxslider.js') }}"></script><!--bx slider-->


<script src="{{ URL::asset('js/custom.js') }}"></script>

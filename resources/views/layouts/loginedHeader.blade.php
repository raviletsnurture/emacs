<header class="header_bg">
  <div class="container-fluid">
    <div class="header_admin_dv">
      <div class="toggle_btn hide_menu"><a href="#"><i class="fa fa-bars"></i></a></div>
      <div class="head_right_sec_dv">
        <ul>
          <li class="dc_online"> <a href="#"><i class="fa fa-angle-down caret_arrow"></i> <span class="online"></span><img src="{{ URL::asset('images/doctor_icon.png') }}"  class="img_responsive_max" alt=""/></a> </li>
          <li class="bell"> <a href="#"> <span class="badge">20</span> <i class="fa fa-bell-o"></i></a> </li>
          <li class="setting"> <a href="#"> <i class="fa fa-gear"></i></a> </li>
          <li class="admin">
            <div class="dropdown"> <a href="#" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="avname_dv"> <span class="caret"></span> Welcome<span class="admin_name"> {{Auth::user()->firstname}} {{Auth::user()->lastname}}</span></div>
              @if(isset($doctorDetails['data']->doctor->doctor_avtar))
                <span class="avtar_box"><img src="{{$doctorDetails['data']->doctor->doctor_avtar}}" class="img_responsive_max"  alt=""/></span> </a>
              @else
                <span class="avtar_box"><img src="{{URL::asset('images/dotor_profile_image.png')}}" class="img_responsive_max"  alt=""/></span> </a>
              @endif
              <ul class="dropdown-menu avtar_sub_menu" aria-labelledby="dLabel">
                <li><a href="{{ url('auth/logout')}}">Log Out</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</header>

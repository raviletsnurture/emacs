<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Dashboard</title>
<!-- favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">

<!-- metis menu -->
<link href="{{ URL::asset('css/metisMenu.css') }}" rel="stylesheet">

<!-- jassney-Bootstrap -->
<link href="{{ URL::asset('css/jasny-bootstrap.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/bootstrap-switch.css') }}" rel="stylesheet">
<!--stylish radio-->
<link rel="stylesheet" href="{{ URL::asset('skins/flat/red.css') }}" type="text/css" media="all">
<!-- Bootstrap -->
<link href="{{ URL::asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/bootstrap-select.css') }}" rel="stylesheet">
<!-- animation css -->
<link href="{{ URL::asset('css/hover.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/animate.css') }}" rel="stylesheet">
<!-- animation css -->

<!-- font awesome -->
<link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/chart.css') }}" rel="stylesheet">
<!-- custom css -->
<link href="{{ URL::asset('css/admin_default.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/admin_responsive.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
<!-- google fonts -->
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js') }} for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js') }} doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
  	<script src="{{ URL::asset('js/additional-methods.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{ URL::to('js/bootbox.min.js') }}"></script>
</head>
<body class="">

<!--upper header-->
@include('layouts.loginedHeader')
<!--upper header-->

<!--side menu-->
@include('layouts.sidebar')
<!--side menu-->

<!--main content-->
@section('content')

@show
<!--main content-->


<!--bootstrap calendar-->
<script src="{{ URL::asset('js/moment-with-locales.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-timepicker.js') }}"></script>


<!--stylish radio-->
<script src="{{ URL::asset('js/icheck.js') }}"></script>
<!-- pie chart-->
<script src="{{ URL::asset('js/jquery.flot.js') }}"></script>
<script src="{{ URL::asset('js/jquery.flot.pie.js') }}"></script>

<!--<script type="text/javascript">

	$(function() {

		var d1 = [];
		for (var i = 0; i < 14; i += 0.5) {
			d1.push([i, Math.sin(i)]);
		}

		var d2 = [[0, 3], [4, 8], [8, 5], [9, 13]];

		// A null signifies separate line segments

		var d3 = [[0, 12], [7, 12], null, [7, 2.5], [12, 2.5]];

		$.plot("#placeholder", [ d1, d2, d3 ]);

		// Add the Flot version string to the footer

		$("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
	});

	</script>-->

<!-- metsi menu-->
<script src="{{ URL::asset('js/metisMenu.js') }}"></script>

<!-- animation-->
<script src="{{ URL::asset('js/wow.js') }}"></script>

<!-- video lightbox-->
<script type="text/javascript" src="{{ URL::asset('html5lightbox/html5lightbox.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--bx slider-->
<script src="{{ URL::asset('js/jquery.bxslider.js') }}"></script>

<!--custom script-->
<script src="{{ URL::asset('js/custom.js') }}"></script>

<!--bootstrap-->


<!--Jasney bootstrap-->
<script src="{{ URL::asset('js/jasny-bootstrap.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-switch.js') }}"></script>
</body>
</html>

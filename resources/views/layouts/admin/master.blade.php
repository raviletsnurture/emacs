<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Emacs Admin</title>
        <link type="text/css" href="{{ URL::asset('css/bootstrap-admin.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/admin.css') }}" rel="stylesheet">        
        <link type="text/css" href="{{ URL::asset('images/icons/css/font-awesome.css') }}" rel="stylesheet">
        <link type="text/css" href="{{ URL::asset('css/bootstrap-wysihtml5.css') }}" rel="stylesheet">


        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    </head>
    <body>
      <input type="hidden" id="host" name="host" value="{{ config('app.url') }}" />
  		@include('layouts.admin.header')

        <div class="wrapper">
            <div class="container">
                  @section('content')
                    @include('layouts.admin.leftSidebar')
                  @show
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->

      @include('layouts.admin.footer')
    </body>
</html>

<div class="span3">
    <div class="sidebar">
        <ul class="widget widget-menu unstyled">
            <li><a href="{{ URL::to('/admin') }}"><i class="menu-icon icon-dashboard"></i>Dashboard
            </a></li>
            <li><a href="{{ URL::to('/admin/page') }}"><i class="menu-icon icon-envelope"></i>Static Pages</a>
            </li>
            <!-- <li><a href="javascript:void(0)"><i class="menu-icon icon-inbox"></i>Inbox <b class="label green pull-right">
                11</b> </a></li>
            <li><a href="javascript:void(0)"><i class="menu-icon icon-tasks"></i>Tasks <b class="label orange pull-right">
                19</b> </a></li> -->
        </ul>
        <!--/.widget-nav-->


        <ul class="widget widget-menu unstyled">
            <!-- <li><a href="javascript:void(0)"><i class="menu-icon icon-book"></i>Manage Users</a></li> -->
            <li><a href="{{ URL::to('/admin/doctors') }}"><i class="menu-icon icon-user-md"></i>Manage Doctors</a></li>
            <li><a href="{{ URL::to('/admin/doctor/speciality') }}"><i class="menu-icon icon-medkit"></i>Manage Doctor Speciality</a></li>
            <li><a href="{{ URL::to('/admin/patients') }}"><i class="menu-icon icon-plus-sign-alt"></i>Manage Patients</a></li>
            <li><a href="{{ URL::to('/admin/pharmacy') }}"><i class="menu-icon icon-table"></i>Manage Pharmacy Listing</a></li>
            <li><a href="javascript:void(0)"><i class="menu-icon icon-table"></i>Manage Video Visit</a></li>
            <li><a href="{{ URL::to('/admin/package') }}"><i class="menu-icon icon-bar-chart"></i>Manage Packages</a></li>

            <li><a href="{{ URL::to('/admin/disease') }}"><i class="menu-icon icon-book"></i>Manage Medical Diseases</a></li>
            <li><a href="{{ URL::to('/admin/testimonial') }}"><i class="menu-icon icon-book"></i>Manage Testimonial</a></li>
            <li><a href="javascript:void(0)"><i class="menu-icon icon-paste"></i>Manage Medicine list</a></li>

            <li><a href="javascript:void(0)"><i class="menu-icon icon-bar-chart"></i>Settings</a></li>
        </ul>

        <!--/.widget-nav-->
        <ul class="widget widget-menu unstyled">
            <!-- <li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
            </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
            </i>More Pages </a>
                <ul id="togglePages" class="collapse unstyled">
                    <li><a href="javascript:void(0)"><i class="icon-inbox"></i>Login </a></li>
                    <li><a href="javascript:void(0)"><i class="icon-inbox"></i>Profile </a></li>
                    <li><a href="javascript:void(0)"><i class="icon-inbox"></i>All Users </a></li>
                </ul>
            </li> -->
            <li><a href="{{ URL::to('auth/logout') }}"><i class="menu-icon icon-signout"></i>Logout </a></li>
        </ul>
    </div>
    <!--/.sidebar-->
</div>
<!--/.span3-->

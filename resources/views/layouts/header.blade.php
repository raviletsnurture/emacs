<header id="sc_top">
  <div class="container">
    <div class="upper_row">
      <div class="logo_main wow bounceInDown"><a href="{{ URL::to('/') }}" title="emacs"><img src="{{ URL::asset('images/logo.png') }}"  class="img_responsive_max" alt=""/></a></div>
      <div class="right_part">
        <div class="signup_btns">
          <ul>
            <li><a href="#">{{ Lang::get('messages.contactus')}} </a></li>

              @if(isset(Auth::user()->user_id))
                  <li class="login">
                    <a href="{{ URL::to('auth/logout') }}">Logout</a>
                  </li>
              @else
                  <li class="login">
                    <a href="{{ URL::to('/signin') }}">{{ Lang::get('messages.login')}}</a>
                  </li>


              @endif

            <li class="blue_btn">
              @foreach (Config::get('languages') as $lang => $language)

    	         	       <a href="{!! route('lang.switch', $lang)  !!}" >{{ $lang }}</a>

    	        @endforeach


             </li>
          </ul>
        </div>
        <div class="navigation_dv">
          <nav class="navbar navbar-default emacs_nav">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav emacs_nav_bar w">
                <li class="active"><a href="#" class="">Home</a></li>
                <li><a href="#" class="hvr-shrink">Medical</a></li>
                <li><a href="#" class="hvr-shrink">Pediatrics</a></li>
                <li ><a href="#" class="hvr-shrink">Psychology</a></li>
                <li><a href="#" class="hvr-shrink">Lactation</a></li>
                <li><a href="#" class="hvr-shrink">Blog</a></li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

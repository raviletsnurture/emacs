<aside class="sidemenu_bg">
  <div class="sidemenu">
    <div class="">
      @if(Auth::user()->getRole()=="patient")
      <a href="{{url('/'.Auth::user()->getRole())}}">
        <div class="side_logo_dv"> <img src="{{ URL::asset('images/admin_logo.png') }}"  class="img_responsive_max" alt=""/>
      </a>
          <div class="video_cal_btn"> <a href="#"><i class="fa fa-video-camera"></i>See a MD Now</a> </div>
        </div>
      @else
      <a href="{{url('/'.Auth::user()->getRole())}}">
        <div class="side_logo_dv"> <img src="{{ URL::asset('images/admin_logo.png') }}"  class="img_responsive_max" alt=""/>
      </a>
          <div class="video_cal_btn"> <a href="#"><i class="fa fa-video-camera"></i>Video Call</a> </div>
        </div>
      @endif
      <div class="side_menu_holder">
        <ul class="metismenu" id="menu">
          <li class="{{Request::is('*/detail/*')?'active':''}}"> <a href="{{url('/'.Auth::user()->getRole())}}" aria-expanded="true"><i class="fa fa-user"></i>My Profile</a> </li>
          @if(Auth::user()->getRole()=="patient")
          <li> <a href="#" ><i class="fa fa-history"></i>Medical History</a> </li>
          <li> <a href="#" ><i class="fa fa-file-text-o"></i>Medicine Record</a> </li>
          <li class="{{Request::is('*/medicalInquiry')?'active':''}}"> <a href="{{url('patient/medicalInquiry')}}"><i class="fa fa-question-circle"></i>Medical Inquiry</a> </li>
          <li class="doctr"> <a href="#" ><i class="fa fa-users"></i>Members</a> </li>
          @endif

          <li class="{{Request::is('*/prescription')?'active':''}}"> <a href="{{ url('doctor/prescription/'.Auth::id()) }}" aria-expanded="true"><i class="fa fa-file-text "></i>Prescriptions</a> </li>
          <li> <a href="#"><i class="fa fa-credit-card"></i>My Account</a> </li>
          <li class="doctr {{Request::is('*/medicalInquiry')?'active':''}}"> <a href="{{url('doctor/medicalInquiry')}}"><i class="fa fa-wechat"></i>Manage Queries</a> </li>
          <li class=""> <a href="#"><i class="fa fa-cogs"></i>Settings</a> </li>
        </ul>
        <div class="copyright_dv">
          <p>Copyright © 2015 EMACS.</p>
        </div>
      </div>
    </div>
  </div>
</aside>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EMACS</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}"><!-- favicon -->
    <link href="{{ URL::asset('css/bootstrap-front.css') }}" rel="stylesheet"><!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap-theme.css') }}" rel="stylesheet"><!-- Bootstrap -->


    <link href="{{ URL::asset('css/jquery.kwicks.css') }}" rel="stylesheet"><!-- kwicks slider -->
    <link href="{{ URL::asset('css/hover.css') }}" rel="stylesheet"><!-- animation css -->
    <link href="{{ URL::asset('css/animate.css') }}" rel="stylesheet"><!-- animation css -->
    <link href="{{ URL::asset('css/jquery.bxslider.css') }}" rel="stylesheet"><!-- testomonial slider -->
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">  <!-- font awesome -->
    <link href="{{ URL::asset('css/default.css') }}" rel="stylesheet"><!-- custom css -->
    <link href="{{ URL::asset('css/responsive.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'><!-- google fonts -->

    @if(App::getLocale() == "ar")
      <link href="{{ URL::asset('css/arabic.css') }}" rel="stylesheet">
      <link href="{{ URL::asset('css/arabic_responsive.css') }}" rel="stylesheet">
    @endif
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

    <body class="{{ (App::getLocale() == "en") ? 'english_lang' : 'arabic_lang' }}">

  		@include('layouts.header')
<!--<input type="hidden" id="host" name="host" value="{{ config('app.url') }}" />-->

                  @section('content')

                  @show

      @include('layouts.footer')
    </body>
</html>

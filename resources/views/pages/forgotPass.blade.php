<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Forgot Pass</title>
	<!-- favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">
	
	<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
	<!-- font awesome -->
	<link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/admin_default.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/admin_responsive.css') }}" rel="stylesheet">
	
	<!-- google fonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
</head>
<body class="">

	<!--upper header-->
	<header class="header_bg">
		<div class="container-fluid">
			<div class="login_logo_dv">
				<img src="{{ URL::asset('images/login_logo.png') }}" class="img_responsive_max" alt="" />
			</div>
		</div>
	</header>
	<div class="login_wrapper">
		<div class="login_container">
			<div class="login_heading">
				
			<h1>Forgot Password</h1>
			</div>
			<form action="{{ URL::to('auth/forgotPass') }}" method="POST">
           		<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="login_bg">
					<div class="login_form_container">
						<h3>Sign in to your account</h3>
						
			@if(Session::has('flash_message'))
              <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Completed!</strong> {{ Session::get('flash_message') }}
              </div>
            @elseif(Session::has('flash_message_error'))
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oops!!</strong> {{ Session::get('flash_message_error') }}
              </div>
            @elseif(count($errors) > 0)
                <div class="alert alert-error">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Oops!!</strong>
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
            @endif
         		<ul>
					<li><input type="text" class="form-control" name=email placeholder="xyz@gmail.com"> 
					</li>
					<li><input type="password" class="form-control"	placeholder="Password" name="password" > 
					<li>
						<button type="submit" >Sign In</button>
					</li>
					<li class="forget_pwd"><a href="{{ URL::to('forgotPass') }}">Forgot Password</a></li>
					<li class="have_account">Don’t have an account yet? <a href="{{ URL::to('/signup') }}">Create an Account</a></li>
				</ul>
			</div>
				</div>
			</form>	
		</div>
	</div>
</body>
</html>

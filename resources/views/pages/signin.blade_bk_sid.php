<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Login</title>
	<!-- favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">
	
	<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
	<!-- font awesome -->
	<link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/admin_default.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/admin_responsive.css') }}" rel="stylesheet">
	
	<!-- google fonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
</head>
<body class="">

	<!--upper header-->
	<header class="header_bg">
		<div class="container-fluid">
			<div class="login_logo_dv">
				<img src="{{ URL::asset('images/login_logo.png') }}" class="img_responsive_max" alt="" />
			</div>
		</div>
	</header>
	<div class="login_wrapper">
		<div class="login_container">
			<div class="login_heading">
				
				@if(Session::has('message'))
					<p class="alert alert-info">{{ Session::get('message') }}</p>
				@endif
				<h1>Welcome</h1>
			</div>
			<form action="{{ URL::to('auth/login') }}" method="POST">
           		<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="login_bg">
					<div class="login_form_container">
						<h3>Sign in to your account</h3>
						
			@if(Session::has('flash_message'))
              <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Completed!</strong> {{ Session::get('flash_message') }}
              </div>
            @elseif(Session::has('flash_message_error'))
              <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Oops!!</strong> {{ Session::get('flash_message_error') }}
              </div>
            @elseif(count($errors) > 0)
                <div class="alert alert-error">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Oops!!</strong>
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
            @endif
         		<ul>
					<li><input type="text" class="form-control" name=email placeholder="xyz@gmail.com"> 
					</li>
					<li><input type="password" class="form-control"	placeholder="Password" name="password" > 
					<li>
						<button type="submit" >Sign In</button>
					</li>
					<li class="forget_pwd"><a href="#forgotPassModal" data-toggle="modal" >Forgot Password</a></li>
					<li class="have_account">Don’t have an account yet? <a href="{{ URL::to('/signup') }}">Create an Account</a></li>
				</ul>
			</div>
				</div>
			</form>	
		</div>
	</div>
	
	<div class="modal fade" id="forgotPassModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        	<span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title" id="exampleModalLabel">Enter your email address and we will send you a link to reset your password.</h4>
		      </div>
		      <div class="modal-body">
		        <form>
		          <input type="hidden" id="host" name="host" value="{{ config('app.url') }}" />
		          <div class="form-group">
		            <label for="recipient-name" class="control-label">Email Address:</label>
		            <input type="text" class="form-control" id="resetPassemail" name="resetPassemail" >
		          </div>
		          
		        </form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary" id="sendEmail" data-remote="sendResetPassLink">Recover My Password	</button>
		      </div>
		    </div>
		  </div>
	</div>

</body>
<script src="{{ URL::asset('js/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	
	var host = $("#host").val();
	var remote = $("#sendEmail").attr("data-remote");
    

	var url = host+'/'+remote;
	
	$("#sendEmail").on("click",function() {

		var email = $("#resetPassemail").val();
		alert(email);
		  $.ajax({
				type: "post",
				data:"email="+email,
				url: url,
				cache: false,
		    	beforeSend: function(data){
					//ajaxProcessLoader(element,"ON");
				},
				success: function(response){
				     // ajaxProcessLoader(element,"OFF");
					console.log(response);
					alert(response);
				      
				}
			});
					
	});

</script>

</html>

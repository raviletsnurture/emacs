<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Login</title>
	<!-- favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('images/favicon.ico') }}">

	<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
	<!-- font awesome -->
	<link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/admin_default.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/admin_responsive.css') }}" rel="stylesheet">

	<!-- google fonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
</head>
<body class="">

	<!--upper header-->
	<header class="header_bg">
		<div class="container-fluid">
			<div class="login_logo_dv">
				<img src="{{ URL::asset('images/login_logo.png') }}" class="img_responsive_max" alt="" />
			</div>
		</div>
	</header>
	<div class="login_wrapper">
		<div class="login_container">
			<div class="login_heading">


				<h1>Reset Password</h1>
			</div>
			<form action="" method="POST">
           		<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" id="unique_code" name="unique_code" value="{{ $code }}">
							<input type="hidden" id="host" name="host" value="{{ config('app.url') }}" />

				<div class="login_bg">
					<div class="login_form_container">
						<h3 id="message">Enter your secure password here.</h3>


         		<ul>

								<li><input type="text" class="form-control newpass"	placeholder="Password" name="newpass"><li>
								<li><input type="text" class="form-control"	placeholder="Confirm Password" name="confpassword" ><li>
									<button type="button" class="btn btn-primary" id="sendEmail" data-remote="resetpass"> Submit	</button>
								</li>

						</ul>
			</div>
				</div>
			</form>
		</div>
	</div>


</body>
<script src="{{ URL::asset('js/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('js/bootbox.min.js') }}"></script>
<script type="text/javascript">

	$(document).ready(function() {

		

		$("#sendEmail").on("click",function() {

			var passw = $(document).find(".newpass").val();
			var unique_code = $("#unique_code").val();
			var url = "{{ URL::to('/resetpass') }}";
			

				$.ajax({
					type: "post",
					data:'{"data":{"unique_code":"'+unique_code+'","newpassword":"'+passw+'"}}',
					url: url,
					//dataType: 'json',
					cache: false,
					beforeSend: function(data){

					},
					success: function(response){
						$(".newpass").removeClass('error');


						obj = JSON.parse(response);
						if(obj.status == "0"){
							$(".newpass").addClass('error');
							$("#message").html(obj.message);
						}
						if(obj.status == "1"){
							$(".newpass").removeClass('error');
							bootbox.alert(obj.message, function() {
								window.location.href = "{{ URL::to('signin') }}";
							});
						}

					},
					error: function(jqXHR, textStatus, errorThrown) {
						$("#message").html(obj.message);

					}
				});

		});

	});


</script>

</html>

@extends('layouts.loginedMaster')
@section('content')

<!--main content-->
<div class="" id="wrapper">
  <div class="content">
    <div class="docter_profile_matter_holder">
      <div class="admin_tittle_name">
        <h3>MY PROFILE</h3>
      </div>
      <div class="doctor_solo_dv">
        @if(isset($doctorDetails['data']->doctor->doctor_avtar))
          <div class="img_dv"><img src="{{$doctorDetails['data']->doctor->doctor_avtar}}"  class="img_responsive_max" alt=""/></div>
        @else
          <div class="img_dv"><img src="{{URL::asset('images/dotor_profile_image.png')}}"  class="img_responsive_max" alt=""/></div>
        @endif
        <div class="img_dv"><img src=""  class="img_responsive_max" alt=""/></div>
        <div class="txt_Dv">
          @if(isset($doctorDetails['data']->firstname) && isset($doctorDetails['data']->lastname))
            <h1>{{$doctorDetails['data']->firstname}} {{$doctorDetails['data']->lastname}}<i class="fa fa-pencil"></i></h1>
          @endif
          @if(isset($doctorDetails['data']->doctor->doctor_speciality))
            <h2>{{$doctorDetails['data']->doctor->doctor_speciality}}</h2>
          @endif
          @if(isset($doctorDetails['data']->city) && isset($doctorDetails['data']->country))
            <h4>{{$doctorDetails['data']->city}}, {{$doctorDetails['data']->country}}</h4>
          @endif
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="profile_boxes_row row">
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Degrees</h3>
              <div class="edit_btns_Dv">
                <a href="#"><i class="fa fa-pencil"></i></a>
                <a href="#" class="mt_plus"> <i class="fa fa-plus"></i></a>
              </div>
            </div>
            @if(isset($degree['data']) && !empty($degree['data']))
              @foreach($degree['data'] as $deg)
              <div class="incresing_row">
                <h1>{{$deg->degree_name}}</h1>
                <h4>{{$deg->college_name}}</h4>
                <p>Passing Year: {{date('Y',strtotime($deg->end_to))}}</p>
              </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Experience</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            @if(isset($experience['data']) && !empty($experience['data']))
              @foreach($experience['data'] as $exp)
              <div class="incresing_row">
                <h1>{{$exp->designation}}</h1>
                <h4>{{$exp->hospital_name}}</h4>
                <p>Experience Years: {{ date('Y',strtotime($exp->end_date)) - date('Y',strtotime($exp->start_date)) }}</p>
              </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Awards</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            @if(isset($award['data']) && !empty($award['data']))
              @foreach($award['data'] as $awd)
              <div class="incresing_row">
                <h1>{{$awd->award_title}}</h1>
                <h4>{{$awd->award_description}}</h4>
              </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Award</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            @if(isset($group['data']) && !empty($group['data']))
              @foreach($group['data'] as $grp)
              <div class="incresing_row">
                <h1>{{$grp->group_title}}</h1>
                <h4>{{$grp->group_description}}</h4>
              </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Research Paper</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            @if(isset($research_paper['data']) && !empty($research_paper['data']))
              @foreach($research_paper['data'] as $research)
              <div class="incresing_row">
                <h1>{{$research->research_paper_title}}</h1>
                <h4>{{$research->research_paper_description}}</h4>
              </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="col-md-4 profile_box_column">
          <div class="profile_box_dv">
            <div class="heading_row">
              <h3>Groups</h3>
              <div class="edit_btns_Dv"> <a href="#"><i class="fa fa-pencil"></i></a> <a href="#" class="mt_plus"> <i class="fa fa-plus"></i> </a> </div>
            </div>
            @if(isset($meeting['data']) && !empty($meeting['data']))
              @foreach($meeting['data'] as $meet)
              <div class="incresing_row">
                <h1>{{$meet->meeting_title}}</h1>
                <h4>{{$meet->meeting_description}}</h4>
              </div>
              @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@extends('layouts.loginedMaster')
@section('content')
<div class="" id="wrapper">
  <div class="content">
    <div class="admin_tittle_name">
      <h3>Write prescriptions</h3>
    </div>
    <div class="row divided_row">
          <!--chart_column-->
          <div class="col-sm-6 new_preciption_column">
            <div class="new_prescipton_dv">
              <div class="conditionla_heading">
                <h1>New Prescription</h1>
              </div>
            <form id="addPresciptionForm" name="addPresciptionForm">
              <input type="hidden" name="doctor_id" value="{{Auth::id()}}">
              <ul class="pre_listing">
                <li>
                  <input class="form-control" placeholder="Prescription Title *" name="prescription_title" type="text">
                </li>
                <li>
                  <select class="form-control full_select" name="patient_id">
                    <option selected value=""> Patient Name * </option>
                    @foreach($patients as $patient)
                      <option value="{{ $patient->user_id }}">{{ $patient->firstname }} {{ $patient->lastname }}</option>
                    @endforeach
                  </select>
                </li>
              </ul>
              </form>
              <ul class="pre_listing">
                <li class="three_lable">
                  <label>
                    <input class="form-control" placeholder="Medicine Name *" type="text" name="medicine_name" id="medicine_name">
                  </label>
                  <label>
                    <select class="form-control error" name="slot" id="slot">
                      <option selected value="Daily"> Daily </option>
                      <option value="Weekly"> Weekly </option>
                      <option value="Biweekly"> Biweekly </option>
                    </select>
                  </label>
                  <label>
                    <input id="timepicker5" type="text" class="input-small form-control" name="time_to_take">
                  </label>
                </li>
                <li>
                  <button class="blue" id="addPrescription">Add Medicine</button>
                </li>
              </ul>
              <div class="addmedi_dv">
                <ul class="add_medilist">

                </ul>
                <div class="save_priscption">
                    <button id="btnSavePrescription" class="pink">Save Prescription</button>
                  </div>
              </div>
            </div>
          </div>

          <!--Prescribed Medicines-->
          <div class="col-sm-6 Prescribed_columnn ">
            <div class="prescribe_holder">
              <div class="prescribe_heading">
                <h1>My Prescriptions</h1>
                <!-- <ul>
                  <li class=""><a class="active" href="#">By Date </a> | <a href="#"> By Patient</a></li>
                </ul> -->
              </div>
              <div class="prescribe_listing_db">
                <ul>
                  @foreach($prescriptionData as $prescription)
                    <li>
                      <a href="javascript:void(0);" class="prescripionView" id="{{$prescription->prescription_id}}">{{$prescription->prescription_title}}</a>
                      <span class="dateStamp pull-right">{{date('d-M-Y',strtotime($prescription->created_at))}}</span>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
  </div>
</div>
<div class="modal fade" id="map_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
           <div class="col-sm-12 Prescribed_columnn prescription-rr ">
        <div class="prescribe_holder">
          <div class="prescribe_heading">
            <h1>My Prescriptions</h1>
          </div>
          <!--prescribe_listing_db-->
          <div class="prescribe_listing_db medical">
            <ul class="loadMedicine">

            </ul>
          </div>
          <!--prescribe_listing_db-->
        </div>
      </div>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<script>

 // $('#map_modal').modal('show');

var memberData = new Array();
var data = {};

$("#addPrescription").on("click",function(event){
    event.preventDefault();
    if(($("#medicine_name").val()).trim()!="" && ($("#slot").val()).trim()!="" && ($("#timepicker5").val()).trim()!=""){
      event.preventDefault();
      $('.three_lable :input').serializeArray().map(
        function(x){memberData[x.name] = x.value;
      });
      //console.log(memberData);
      memberData.push({
        "medicine_name" : memberData.medicine_name,
        "slot" : memberData.slot,
        "time_to_take" : memberData.time_to_take,
      })
      // console.log(memberData);
      var len = $(".add_medilist li").length;
      console.log(len);
      $(".add_medilist").append("<li class=''><label><label value='' class='tagLebel'>"+memberData.medicine_name+"</label><a href='javascript:void(0);' class='deleteMember' onclick='deleteMember(this);' id="+len+"><i class='fa fa-close cross_red'></i></a></label><label class='daily_bn'>"+memberData.slot+"</label><label class='time_btn'>"+memberData.time_to_take+"</label></li>");
    }else {
      bootbox.alert("Please enter medicine's details");
    }
});
$("#addPresciptionForm").validate({
    onfocusout: false,
      invalidHandler: function(form, validator) {
          var errors = validator.numberOfInvalids();
          if (errors) {
              validator.errorList[0].element.focus();
          }
      },
    ignore: [],
    debug: false,
    rules: {
      prescription_title:{
        required: true,
      },
      patient_id:{
        required:true,
      },
    },
    messages:{
      medicine_name:{
        required: 'Please enter first name',
      },
      patient_id:{
        required: 'Please select patient name'
      },
    },
  });
$("#btnSavePrescription").on("click",function(event){
  event.preventDefault();
  if($("#addPresciptionForm").valid())
  {
    // console.log($("#addPresciptionForm").serialize());
    var formdata = $("#addPresciptionForm").serializeArray().map(
        function(x){data[x.name] = x.value;
     });
     data['medicines'] = memberData;
    //  console.log(data);

     $.ajax({
   		type: "POST",
   		url: "{{url('/'.'api/prescription')}}",
   		cache: false,
   		data: '{"data": '+JSON.stringify(data)+'}',
   		dataType: "json",
   		beforeSend: function(data){
   			// ajaxProcessLoader(element,"ON");
   		},
   		success: function(response){
         if(response.status=="1"){
           bootbox.alert(response.message,function(e){
              document.forms['addPresciptionForm'].reset();
              $("#medicine_name").val("");
              window.location.reload();
           });

         }
   		}
   	});
  }
});

$(".prescripionView").on("click",function(){
    var prescription_id = $(this).attr("id");
    // console.log(JSON.stringify(id));
   //$('#map_modal').modal('show');
   var ary = {};
   ary = {
     "prescription_id" : prescription_id,
   };
   console.log(ary);
   $.ajax({
     type: "POST",
     url: "{{url('/'.'api/prescription/details')}}",
     cache: false,
     data: '{"data": '+JSON.stringify(ary)+'}',
     dataType: "json",
     beforeSend: function(data){
       // ajaxProcessLoader(element,"ON");
     },
     success: function(response){
      //  console.log(response);
        if(response.status=="1"){
          $('#map_modal').modal('show');

          setTimeout(function(){
            // set title of model
            $(document).find("#myModalLabel").html(response.data.firstname+" "+response.data.lastname+" ("+response.data.prescription_title+")");

            $(document).find(".loadMedicine").html("");

            var medicineArrayLen = response.data.medicines.length;
            for(var i=0;i<medicineArrayLen;i++){
              console.log(i);
              $(document).find(".loadMedicine").append("<li>"+response.data.medicines[i].medicine_name+"<span>"+response.data.medicines[i].slot+"</span><span> - </span><span>"+response.data.medicines[i].time_to_take+"</li>");
            }

          },100);
        }
     }
   });

});

function deleteMember(obj){
  var index = $(obj).attr("id");
  memberData.splice(index,1);
  $(obj).parents('li').remove();
  // data['members'].remove(index);
  var child=1;
  var len = $(".add_medilist li").length;
  for(var i=0;i<=len;i++){
    $(".add_medilist li:nth-child("+child+") a").attr("id",i)
    child = child + 1;
  }
  console.log(memberData);
}

</script>
@stop

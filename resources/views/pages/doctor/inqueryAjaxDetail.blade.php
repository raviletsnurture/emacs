<li>
  <!--toptxt-->
  <div class="toptxt"> <span class="name">{{$queryData[0]->firstname}} {{$queryData[0]->lastname}}</span> <span class="pull-right">{{date('d/m/y h:i A',strtotime($queryData[0]->created_at))}}</span> </div>
  <!--toptxt-->
  <div class="desci">
    <p id="queryTitle">{{$queryData[0]->query_title}}</p>
  </div>
  <div class="desci half">
    <p id="queryTextByPatient">{{$queryData[0]->text}}</p>
  </div>
</li>
@if(isset($queryData[0]->reply) && !empty($queryData[0]->reply))
  @foreach($queryData as $reply)
    <li>
      <!--toptxt-->
      <div class="toptxt"> <span class="name">{{$reply->replyerFName}} {{$reply->replyerLName}}</span> <span class="pull-right">{{date('d/m/y h:i A',strtotime($reply->created_at))}}</span> </div>
      <!--toptxt-->
      <div class="desci half">
        <p>{{$reply->reply}}</p>
      </div>
    </li>
  @endforeach
@endif
<li>
  <textarea name="text" class="form-control" placeholder="Type your answer" type="text"></textarea>
</li>

<li>
  <input type="button" class="btn1 save myBtn" value="Reply" id="btnReply"/>
</li>

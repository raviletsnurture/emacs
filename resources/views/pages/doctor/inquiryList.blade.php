@extends('layouts.loginedMaster')
@section('content')
<div class="" id="wrapper">
  <div class="content">
    <div class="admin_tittle_name">
      <h3>MEDICAL INQUIRY</h3>
    </div>
    <div class="row medical_condition_row">

      <!--chart_column-->
      <form name="frmInquiryReply" id="frmInquiryReply">
        <input type="hidden" value="{{ csrf_token() }}" id="token" name="token">
        <input type="hidden" value="{{Auth::user()->user_type}}" name="msg_type">
        <input type="hidden" value="{{Auth::user()->user_id}}" name="user_id">
        <input type="hidden" name="parent_id" id="parent_id">
        <div class="col-sm-6 medical_dondition_column">
          <div class="conditionla_heading">
            <h1>Patient Query</h1>
          </div>
          <div class="new_prescipton_dv">
            <!--medical-inquiryview-->
            <div class="medical-inquiryview">
              <ul class="doctorReplayDiv pre_listing">

              </ul>
            </div>
            <!--medical-inquiryview-->

          </div>
        </div>
    </form>
      <!--Prescribed Medicines-->
      <div class="col-sm-6 Prescribed_columnn ">
        <div class="prescribe_holder">
          <div class="prescribe_heading">
            <h1>Queries</h1>
            <!-- <ul>
              <li class=""><a href="#" class="active">By Date</a> | <a href="#"> By Doctor</a></li>
            </ul> -->
          </div>

          <!--prescribe_listing_db-->
          <div class="prescribe_listing_db medical">
            <ul>
              @foreach($returnData as $inquiry)
              @if(isset($inquiry->query_title) && !empty($inquiry->query_title))
                <li><a href="javascript:void(0);" class="inquiryListItem" data="{{$inquiry->msg_id}}">{{$inquiry->query_title}}</a><span>{{date('d-M-Y',strtotime($inquiry->created_at))}}</span></li>
              @endif
              @endforeach
            </ul>
          </div>
          <!--prescribe_listing_db-->
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	window.onload=function(){
		$('.selectpicker').selectpicker();
	};
  $(document).ready(function(){

    $("#frmInquiryReply").validate({
        onfocusout: false,
          invalidHandler: function(form, validator) {
              var errors = validator.numberOfInvalids();
              if (errors) {
                  validator.errorList[0].element.focus();
              }
          },
        ignore: [],
        debug: false,
        rules: {
          text:{
            required: true
          },
        },
        query_title:{
          text:{
            required: 'Please enter content',
          },
        },
      });



    $(document).on("click","#btnReply",function(){
      if($("#frmInquiryReply").valid()){
        var dataString = $("#frmInquiryReply").serialize();
        console.log(dataString);
        $.ajax({
          type: "POST",
          url: "{{url()}}/api/query",
          data: dataString,
          dataType: "json",
          beforeSend:function(){

          },
          success:function(data){
            if(data.status){
              window.location.reload();
              $("#frmInquiryReply").find("input[type='text']").val("");
              $("#frmInquiryReply").find("#member_id").val("");
              $("#frmInquiryReply").find("textarea").val("");
            }
          }
        });
      }
    });

    $(".inquiryListItem").on("click",function(){
      // set parent_id
      var query_id = $(this).attr("data");
      $("#parent_id").val(query_id);

      var dataString = "query_id="+query_id;
      $.ajax({
        type: "POST",
        url: "details",
        data: dataString,
        dataType: "json",
        beforeSend:function(){

        },
        success:function(data){
          console.log(data);
          if(data!="0"){
            $(".doctorReplayDiv").html(data.html);
          }else {
            $(".doctorReplayDiv").html("");
            $(".doctorReplayDiv").html("No content found");
          }
        }
      })
    });
  });
</script>
@stop

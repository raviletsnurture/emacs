@extends('layouts.admin.master')

@section('content')

<div class="row">

    <!-- call section in master layout to include sidebar -->
    @parent

    <div class="span9">
        <div class="content">

            <!--/.Docters module-->
            <div class="module">
                <div class="module-head">
                    <h3> Doctors <a class="btn btn-small pull-right addButton" data-toggle="modal" data-new-remote="/admin/doctors" data-target="#AddFormModal"> <i class="icon-plus icon-1"></i> Add New</a></h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="usersDatatable table table-bordered table-striped	display" width="100%">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Fullname </th>
                                <th> Email </th>

                                <th> Verified </th>
                                <th> Status </th>
                                <th> Registered On </th>
                                <th> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                          <?php $i = 1; ?>
                          @foreach ($doctors as $doctor)
                            @if($i % 2 == 0)
                              <tr class="even">
                                <td>{{ $doctor->user_id }}</td>
                                <td>{{ $doctor->firstname }} {{ $doctor->lastname }}</td>
                                <td>{{ $doctor->email }}</td>

                                <td>
                                  @if($doctor->is_verified)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $doctor->user_id }}"> Verified </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-standard' data-id="{{ $doctor->user_id }}"> Unverified </span>
                                  @endif
                                </td>
                                <td>
                                  @if($doctor->status)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $doctor->user_id }}"> Active </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-danger'  data-id="{{ $doctor->user_id }}"> Inactive </span>
                                  @endif
                                </td>
                                <td>{{ $doctor->created_at }}</td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/doctors" data-toggle="modal" data-modal-title="Edit Doctor" data-target="#EditFormModal" data-id="{{ $doctor->user_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/doctors" data-target="#Delete" data-id="{{ $doctor->user_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @else
                              <tr class="odd">
                                <td>{{ $doctor->user_id }}</td>
                                <td>{{ $doctor->firstname }} {{ $doctor->lastname }}</td>
                                <td>{{ $doctor->email }}</td>

                                <td>
                                  @if($doctor->is_verified)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $doctor->user_id }}"> Verified </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-standard' data-id="{{ $doctor->user_id }}"> Unverified </span>
                                  @endif
                                </td>
                                <td>
                                  @if($doctor->status)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $doctor->user_id }}"> Active </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-danger'  data-id="{{ $doctor->user_id }}"> Inactive </span>
                                  @endif
                                </td>
                                <td>{{ $doctor->created_at }}</td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/doctors" data-toggle="modal" data-modal-title="Edit Doctor" data-target="#EditFormModal" data-id="{{ $doctor->user_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/doctors" data-target="#Delete" data-id="{{ $doctor->user_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @endif
                              <?php $i++; ?>

                          @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                              <th> # </th>
                              <th> Fullname </th>
                              <th> Email </th>

                              <th> Verified </th>
                              <th> Status </th>
                              <th> Registered On </th>
                              <th> Actions </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--/.module-->

        </div>
        <!--/.content-->
    </div>
    <!--/.span9-->
</div>

<!-- Modal -->
<div id="AddFormModal" class="modal module fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header module-head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Doctor Details</h4>
      </div>

      <div class="modal-body module-body">
        <div class="content">
            <form class="form-horizontal row-fluid AddForm">
              <input type="hidden" name="user_type" value="1" />

              <div class="control-group">
                <label class="control-label" for="firstname">First Name *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="firstname" class="span8 firstname">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="lastname">Last Name</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="lastname" class="span8 lastname">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="speciality">Speciality</label>
                <div class="controls">
                
                  <select name="doctor_speciality">
                    {{ <?php foreach ($speciality as $specialities): ?>
                        <option value="{{ $specialities->doctor_specialization }}">{{ $specialities->doctor_specialization }}</option>
                    <?php endforeach; ?> }}
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="username">Username *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="username" class="span8 username">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="email">Email *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="email" class="span8 email">
                </div>
              </div>

              <!-- <div class="control-group">
                <label class="control-label" for="password">Password</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="password" class="span8 password">
                </div>
              </div> -->

              <!-- <div class="control-group">
                <label class="control-label" for="user_type">User Type</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="user_type" class="span8 user_type">
                    <option value="1">Doctor</option>
                    <option value="2">User</option>
                  </select>
                </div>
              </div> -->

              <div class="control-group">
                <label class="control-label" for="is_verified">Verified</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="is_verified" class="span8 is_verified">
                    <option value="1">Verified</option>
                    <option value="0">Unverified</option>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="is_blocked">Blocked</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="is_blocked" class="span8 is_blocked">
                    <option value="1">Blocked</option>
                    <option value="0" selected>Unblocked</option>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="status">Status</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="status" class="span8 status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
              </div>

            </form>
        </div><!--/.content-->
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btnSaveRecord">Save</button>
      </div>

    </div>
  </div>

</div>
@endsection

@extends('layouts.admin.master')

@section('content')

<div class="row">

    <!-- call section in master layout to include sidebar -->
    @parent

    <div class="span9">
        <div class="content">

            <!--/.Docters module-->
            <div class="module">
                <div class="module-head">
                    <h3> Pharmacy <a class="btn btn-small pull-right addButton" data-toggle="modal" data-new-remote="/admin/pharmacy" data-target="#AddFormModal"> <i class="icon-plus icon-1"></i> Add New</a></h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="usersDatatable table table-bordered table-striped	display" width="100%">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Name </th>
                                <th> City </th>
                                <th> Country </th>
                                <th> Zip </th>
                                <th> Registered On </th>
                                <th> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                          <?php $i = 1; ?>
                          @foreach ($pharma as $pharmacy)
                            @if($i % 2 == 0)
                              <tr class="even">
                                <td>{{ $pharmacy->pharmacy_id }}</td>
                                <td>{{ $pharmacy->pharmacy_name }} </td>
                                <td>{{ $pharmacy->pharmacy_city }}</td>
                                <td>{{ $pharmacy->pharmacy_country }}</td>
                                <td>{{ $pharmacy->pharmacy_zip }}</td>
                                <td>{{ $pharmacy->created_at }}</td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/pharmacy" data-toggle="modal" data-modal-title="Edit pharmacy" data-target="#EditFormModal" data-id="{{ $pharmacy->pharmacy_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/pharmacy" data-target="#Delete" data-id="{{ $pharmacy->pharmacy_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @else
                              <tr class="odd">
                                <td>{{ $pharmacy->pharmacy_id }}</td>
                                <td>{{ $pharmacy->pharmacy_name }} </td>
                                <td>{{ $pharmacy->pharmacy_city }}</td>
                                <td>{{ $pharmacy->pharmacy_country }}</td>
                                <td>{{ $pharmacy->pharmacy_zip }}</td>
                                <td>{{ $pharmacy->created_at }}</td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/pharmacy" data-toggle="modal" data-modal-title="Edit pharmacy" data-target="#EditFormModal" data-id="{{ $pharmacy->pharmacy_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/pharmacy" data-target="#Delete" data-id="{{ $pharmacy->pharmacy_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @endif
                              <?php $i++; ?>

                          @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                              <th> # </th>
                              <th> Name </th>
                              <th> City </th>
                              <th> Country </th>
                              <th> Zip </th>
                              <th> Registered On </th>
                              <th> Actions </th>
                          </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--/.module-->

        </div>
        <!--/.content-->
    </div>
    <!--/.span9-->
</div>

<!-- Modal -->
<div id="AddFormModal" class="modal module fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header module-head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pharmacy Details</h4>
      </div>

      <div class="modal-body module-body">
        <div class="content">
            <form class="form-horizontal row-fluid AddForm">
              <!-- <input type="hidden" name="user_type" value="1" /> -->

              <div class="control-group">
                <label class="control-label" for="firstname">Name *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_name" class="span8 pharmacy_name">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="lastname">Address Line 1</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_addr1" class="span8 pharmacy_addr1">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="lastname">Address Line 2</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_addr2" class="span8 pharmacy_addr2">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="username">City </label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_city" class="span8 pharmacy_city">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="email">State </label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_state" class="span8 pharmacy_state">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="email">Country   </label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_country" class="span8 pharmacy_country">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="email">Zip code </label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="pharmacy_zip" class="span8 pharmacy_zip">
                </div>
              </div>

          </form>
        </div><!--/.content-->
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btnSaveRecord">Save</button>
      </div>

    </div>
  </div>

</div>
@endsection

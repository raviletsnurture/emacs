<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Emacs Admin</title>
    <link type="text/css" href="{{ URL::asset('css/bootstrap-admin.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('css/admin.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ URL::asset('images/icons/css/font-awesome.css') }}" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
					<i class="icon-reorder shaded"></i>
				</a>

        <a class="brand" href="{{ URL::to('/') }}">Emacs Admin</a>

				<!-- <div class="nav-collapse collapse navbar-inverse-collapse">

					<ul class="nav pull-right">

						<li><a href="#">
							Sign Up
						</a></li>



						<li><a href="#">
							Forgot your password?
						</a></li>
					</ul>
				</div><!-- /.nav-collapse -->
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->

	<div class="wrapper">
		<div class="container">

			<div class="row">
				<div class="module module-login span4 offset4">
					<form class="form-vertical" action="{{ URL::to('auth/login') }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="module-head">
							<h3>Sign In</h3>
						</div>

						<div class="module-body">
             @if(Request::session()->has('flash_message'))
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Completed!</strong> {{ Request::session()->pull('flash_message') }}
                </div>
              @elseif(Request::session()->has('flash_message_error'))
                <div class="alert alert-error">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Oops!!</strong> {{ Request::session()->pull('flash_message_error') }}
                </div>
              @elseif(count($errors) > 0)
                <div class="alert alert-error">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Oops!!</strong>
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif

							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="text" id="inputEmail" name="email" placeholder="Email">
								</div>
							</div>
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="password" id="inputPassword" name="password" placeholder="Password">
								</div>
							</div>
						</div>

						<div class="module-foot">
							<div class="control-group">
								<div class="controls clearfix">
									<button type="submit" class="btn btn-primary pull-right">Login</button>
								</div>
							</div>
						</div>

					</form>

				</div>
			</div>
		</div>
	</div><!--/.wrapper-->

  <div class="footer">
      <div class="container">
          <b class="copyright">&copy; 2015 Emacs </b>All rights reserved.
      </div>
  </div>
  <script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('js/jquery-ui-1.10.1.custom.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ URL::asset('js/functions.js') }}" type="text/javascript"></script>
</body>

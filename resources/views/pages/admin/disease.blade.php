@extends('layouts.admin.master')

@section('content')

<div class="row">

    <!-- call section in master layout to include sidebar -->
    @parent

    <div class="span9">
        <div class="content">

            <!--/.Docters module-->
            <div class="module">
                <div class="module-head">
                    <h3> Disease <a class="btn btn-small pull-right addButton" data-toggle="modal" data-new-remote="/admin/disease" data-target="#AddFormModal"> <i class="icon-plus icon-1"></i> Add New</a></h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="usersDatatable table table-bordered table-striped	display" width="100%">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Disease </th>
                                <th> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                          @foreach ($types as $type)
                            @if($i % 2 == 0)
                              <tr class="even">
                                <td>{{ $type->disease_id }}</td>
                                <td>{{ $type->disease_name }} </td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/disease" data-toggle="modal" data-modal-title="Edit Doctor" data-target="#EditFormModal" data-id="{{ $type->disease_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/disease" data-target="#Delete" data-id="{{ $type->doctor_type_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @else
                              <tr class="odd">
                                <td>{{ $type->disease_id }}</td>
                                <td>{{ $type->disease_name }} </td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/disease" data-toggle="modal" data-modal-title="Edit Doctor" data-target="#EditFormModal" data-id="{{ $type->disease_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/disease" data-target="#Delete" data-id="{{ $type->doctor_type_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @endif
                              <?php $i++; ?>

                          @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                              <th> # </th>
                              <th> Disease </th>
                              <th> Actions </th>
                          </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--/.module-->

        </div>
        <!--/.content-->
    </div>
    <!--/.span9-->
</div>

<!-- Modal -->
<div id="AddFormModal" class="modal module fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header module-head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Disease</h4>
      </div>

      <div class="modal-body module-body">
        <div class="content">
            <form class="form-horizontal row-fluid AddForm">
              

              <div class="control-group">
                <label class="control-label" for="doctor_specialization">Disease *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="disease_name" class="span8 disease_name">
                </div>
              </div>

            </form>
        </div><!--/.content-->
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btnSaveRecord">Save</button>
      </div>

    </div>
  </div>

</div>
@endsection

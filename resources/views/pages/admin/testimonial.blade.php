@extends('layouts.admin.master')

@section('content')

<div class="row">

    <!-- call section in master layout to include sidebar -->
    @parent

    <div class="span9">
        <div class="content">

            <!--/.Docters module-->
            <div class="module">
                <div class="module-head">
                    <h3> Testimonial <a class="btn btn-small pull-right addButton" data-toggle="modal" data-new-remote="/admin/testimonial" data-target="#AddFormModal"> <i class="icon-plus icon-1"></i> Add New</a></h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="usersDatatable table table-bordered table-striped	display" width="100%">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Avtar </th>
                                <th> By </th>
                                <th> Post </th>
                                <th> Testimonial text</th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                          @foreach ($types as $type)
                            @if($i % 2 == 0)
                              <tr class="even">
                              	<td>{{ $type->testimonial_id }}</td>
                                <td><img height="100" width="100" alt="Avtar" src="{{ url()."/images/upload/".$type->testimonial_user_image_url }}"></td>
                                <td>{{ $type->testimonial_by_user_name }} </td>
                                <td>{{ $type->testimonial_by_post }} </td>
                                <td>{{ $type->testimonial_text }} </td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/testimonial" data-toggle="modal" data-modal-title="Edit Doctor" data-target="#EditFormModal" data-id="{{ $type->testimonial_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/testimonial" data-target="#Delete" data-id="{{ $type->testimonial_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @else
                              <tr class="odd">
                               	<td>{{ $type->testimonial_id }}</td>
                                <td><img height="100" width="100" alt="Avtar" src="{{ url()."/images/upload/".$type->testimonial_user_image_url }}"></td>
                                <td>{{ $type->testimonial_by_user_name }} </td>
                                <td>{{ $type->testimonial_by_post }} </td>
                                <td>{{ $type->testimonial_text }} </td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/testimonial" data-toggle="modal" data-modal-title="Edit Doctor" data-target="#EditFormModal" data-id="{{ $type->testimonial_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/testimonial" data-target="#Delete" data-id="{{ $type->testimonial_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @endif
                              <?php $i++; ?>

                          @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                             	<th> # </th>
                                <th> Avtar </th>
                                <th> By </th>
                                <th> Post </th>
                                <th> Testimonial text</th>
                                <th> Action </th>
                          </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--/.module-->

        </div>
        <!--/.content-->
    </div>
    <!--/.span9-->
</div>

<!-- Modal -->
<div id="AddFormModal" class="modal module fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header module-head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Testimonial</h4>
      </div>

      <div class="modal-body module-body">
        <div class="content">
            <form class="form-horizontal row-fluid AddForm AddAllForm" id="testimonial_form" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
              <div class="control-group">
                <label class="control-label" for="doctor_specialization">user image </label>
                <div class="controls">
                  <input type="file" name="tempath" class="span8 tempath" id="testimg" accept="image/*">
                  <img src="{{ url().'/images/upload/' }}" height="100" width="100" title="user_image" id="formViewImage" style="display:none;" >
                  <!-- <input type="hidden" name="testimonial_user_image_url" id="testimonial_user_image_url"> -->
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="doctor_specialization">Text *</label>
                <div class="controls">
                  <textarea placeholder="Type something here..." id="testitxt" name="testimonial_text" class="span8 testimonial_text"></textarea>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="doctor_specialization">By Username *</label>
                <div class="controls">
                	<input type="text" name="testimonial_by_user_name" class="span8 testimonial_by_user_name">
                </div>
              </div>

          	  <div class="control-group">
                <label class="control-label" for="doctor_specialization">User Post *</label>
                <div class="controls">
                	<input type="text" name="testimonial_by_post" class="span8 testimonial_by_post">
                </div>
              </div>

            </form>
        </div><!--/.content-->
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btnSaveAllRecord">Save</button>
      </div>

    </div>
  </div>

</div>
@endsection

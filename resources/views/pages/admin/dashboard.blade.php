@extends('layouts.admin.master')

@section('content')

<div class="row">

    <!-- call section in master layout to include sidebar -->
    @parent

    <div class="span9">
        <div class="content">

            <!--/.Users module-->
            <div class="module">
                <div class="module-head">
                    <h3> Patients <a class="btn btn-small pull-right addButton" data-toggle="modal" data-new-remote="/admin/patients" data-target="#AddFormModal"> <i class="icon-plus icon-1"></i> Add New</a></h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="usersDatatable table table-bordered table-striped	display" width="100%">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Fullname </th>
                                <th> Email </th>
                                <th> Disease </th>
                                <th> Verified </th>
                                <th> Status </th>
                                <th> Registered On </th>
                                <th> Actions </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; ?>
                          @foreach ($users as $user)
                            @if($i % 2 == 0)
                              <tr class="even">
                                <td>{{ $user->user_id }}</td>
                                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->	disease_name }}</td>

                                <td>
                                  @if($user->is_verified)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $user->user_id }}"> Verified </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-standard' data-id="{{ $user->user_id }}"> Unverified </span>
                                  @endif
                                </td>
                                <td>
                                  @if($user->status)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $user->user_id }}"> Active </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-danger'  data-id="{{ $user->user_id }}"> Inactive </span>
                                  @endif
                                </td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/patients" data-toggle="modal" data-modal-title="Edit User" data-target="#EditFormModal" data-id="{{ $user->user_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/patients" data-target="#Delete" data-id="{{ $user->user_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @else
                              <tr class="odd">
                                <td>{{ $user->user_id }}</td>
                                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->	disease_name }}</td>
                                <td>
                                  @if($user->is_verified)
                                    <span class='btn btn-mini btn-xs btn-success' data-id="{{ $user->user_id }}"> Verified </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-standard' data-id="{{ $user->user_id }}"> Unverified </span>
                                  @endif
                                </td>
                                <td>
                                  @if($user->status)
                                      <span class='btn btn-mini btn-xs btn-success' data-id="{{ $user->user_id }}"> Active </span>
                                  @else
                                    <span class='btn btn-mini btn-xs btn-danger'  data-id="{{ $user->user_id }}"> Inactive </span>
                                  @endif
                                </td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                  <a class="pull-left addButton icon-edit" data-remote="/admin/patients" data-toggle="modal" data-modal-title="Edit User" data-target="#EditFormModal" data-id="{{ $user->user_id }}"></a>
                                  <a class="pull-left addButton icon-remove" data-remote="/admin/patients" data-target="#Delete" data-id="{{ $user->user_id }}" style="color:#d14;"></a>
                                </td>
                              </tr>
                            @endif
                              <?php $i++; ?>

                          @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                              <th> # </th>
                              <th> Fullname </th>
                              <th> Email </th>
                              <th> Disease </th>
                              <th> Verified </th>
                              <th> Status </th>
                              <th> Registered On </th>
                              <th> Actions </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!--/.module-->

        </div>
        <!--/.content-->
    </div>
    <!--/.span9-->
</div>

<!-- Modal -->
<div id="AddFormModal" class="modal module fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header module-head">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Patient Details</h4>
      </div>

      <div class="modal-body module-body">
        <div class="content">
            <form class="form-horizontal row-fluid AddForm">
              <input type="hidden" name="user_type" value="2" />



              <div class="control-group">
                <label class="control-label" for="firstname">First Name *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="firstname" class="span8 firstname">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="lastname">Last Name</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="lastname" class="span8 lastname">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="username">Username *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="username" class="span8 username">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="email">Email *</label>
                <div class="controls">
                  <input type="text" placeholder="Type something here..." name="email" class="span8 email">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="disease_name">Disease</label>
                <div class="controls">

                  <select name="disease_name">
                    {{ <?php foreach ($disease as $disaeses): ?>
                        <option value="{{ $disaeses->disease_name }}">{{ $disaeses->disease_name }}</option>
                    <?php endforeach; ?> }}
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="is_verified">Verified</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="is_verified" class="span8 is_verified">
                    <option value="1">Verified</option>
                    <option value="0">Unverified</option>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="is_blocked">Blocked</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="is_blocked" class="span8 is_blocked">
                    <option value="1">Blocked</option>
                    <option value="0" selected>Unblocked</option>
                  </select>
                </div>
              </div>

              <div class="control-group">
                <label class="control-label" for="status">Status</label>
                <div class="controls">
                  <select tabindex="1" data-placeholder="Select here.." name="status" class="span8 status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
              </div>

            </form>
        </div><!--/.content-->
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btnSaveRecord">Save</button>
      </div>

    </div>
  </div>

</div>
@endsection

@extends('layouts.master')

@section('content')

<section>
		<div class="banner_section">
			<div class="banner_bg">
				<div class="container">
					<div class="emerG_right_box">
						<div class="emer_inner_box">
							<div class="dctr_online_row">
								<div class="dctr_column_1">
									<h1>
										200 <span>Doctors Online</span>
									</h1>
								</div>
								<div class="dctr_column_2">
									<h1>
										300 <span>Satisfied Patients</span>
									</h1>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="emergency_md_row">
								<h4>Emergency medicine and acute care solutions</h4>
								<a href="#"> CHECK IT OUT</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- ==============================Banner end here -->

	<!-----------------------------------kwicrks slider -->

	<section>


		<div class="flexslider">
			<ul class="slides">
				<li><a href="#"><img src="images/slider_img_4.jpg"
						alt="Join us" />
				</a>
					<div class="flex-caption">
						<div class="bottom_text_dv">
							<h3>
								<i class="fa fa-user-plus"></i>Join us
							</h3>
						</div>
					</div></li>
				<li><a href="#"><img src="images/slider_img_1.jpg"
						alt="Book an appointment" />
				</a>
					<div class="flex-caption">
						<div class="bottom_text_dv bg_2">
							<h3>
								<i class="fa fa-check-circle"></i>Book an appointment
							</h3>
						</div>
					</div></li>
				<li><a href="#"><img src="images/slider_img_2.jpg"
						alt="Video chat with us" />
				</a>
					<div class="flex-caption">
						<div class="bottom_text_dv bg_3">
							<h3>
								<i class="fa fa-video-camera"></i>Video chat with us
							</h3>
						</div>
					</div></li>
				<li><a href="#"><img src="images/slider_img_3.jpg"
						alt="meet our team" />
				</a>
					<div class="flex-caption">
						<div class="bottom_text_dv bg_4">
							<h3>
								<i class="fa fa-users"></i>meet our team
							</h3>
						</div>
					</div></li>
			</ul>
		</div>
		<span id="responsiveFlag"></span>
	</section>


	<!-----------------------------------what is emac start -->

	<section class="What_is_emac_sec">
		<div class="container">
			<div class="whatis_emac_dv">
				<h4>What is emacs</h4>
				<div class="what_emac_row row">
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_1.png" alt="" />
							<h5>Access to Top Health Care Professionals</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_2.png" alt="" />
							<h5>Maintain Your Medical History</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_3.png" alt="" />
							<h5>Maintain Your Medicine Record</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_4.png" alt="" />
							<h5>Get Prescriptions for Your Problems</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_5.png" alt="" />
							<h5>View List of Nearby Pharmacies</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_6.png" alt="" />
							<h5>Pay Per Call</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_7.png" alt="" />
							<h5>Manage Your Family Members</h5>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="waht_emac_coluimn">
							<img src="images/what_icon_8.png" alt="" />
							<h5>On-demand or on your schedule</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-----------------------------------what is emac end -->

	<!-----------------------------------when to emmac start -->

	<section class="use_emac_Sec">
		<div class="container">
			<div class="use_emac_blue_row row">
				<div
					class="col-md-5 col-sm-8 pull-right useemac_blue_column col-xs-12">
					<div class="useemac_blue_column_bg">
						<div class="bluebg_inner_column">
							<h4>when to use emacs</h4>
							<ul class="useemac_listing">
								<li><span><img src="images/whenic_1.png"
										class="img_responsive_max" alt="When u are sick" />
								</span>
									<h5>When you are sick</h5></li>
								<li><span><img src="images/whenic_2.png"
										class="img_responsive_max" alt="When u are sick" />
								</span>
									<h5>When you’re well</h5></li>
								<li><span><img src="images/whenic_3.png"
										class="img_responsive_max" alt="When u are sick" />
								</span>
									<h5>When you need a new doctor</h5></li>
								<li><a href="#">Book an appointment</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-----------------------------------when to emmac end-->

	<!-----------------------------------testomonial section -->

	<section>
		<div class="container">
			<div class="testomonial_dv">
				<div class="testo_heading">
					<h1>Meet our doctors</h1>
				</div>
				<div class="testoslideR_dv">
					<div class="bxslider">
						<div class="item_slide">
							<div class="img_right">
								<img src="images/meet_dctr_img.jpg"
									class="img_responsive_max wow bounceIn" alt="" />
							</div>
							<div class="text_left">
								<h3>Dr. Erik Bana</h3>
								<span>M.D. Pediatrics </span>
								<p>“ Lorem Ipsum is simply dummy text of the printing and
									typesetting industry. Lorem Ipsum has been the industry's
									standard dummy text ever since the 1500s, when an unknown print
									took a galley of type and scrambled it to make a type specimen
									book. ”</p>
								<a href="#">join us as a doctor</a>
							</div>
						</div>
						<div class="item_slide">
							<div class="img_right">
								<img src="images/meet_dctr_img.jpg"
									class="img_responsive_max wow bounceIn" alt="" />
							</div>
							<div class="text_left">
								<h3>Dr. Erik Bana</h3>
								<span>M.D. Pediatrics </span>
								<p>“ Lorem Ipsum is simply dummy text of the printing and
									typesetting industry. Lorem Ipsum has been the industry's
									standard dummy text ever since the 1500s, when an unknown print
									took a galley of type and scrambled it to make a type specimen
									book. ”</p>
								<a href="#">join us as a doctor</a>
							</div>
						</div>
						<div class="item_slide">
							<div class="img_right">
								<img src="images/meet_dctr_img.jpg"
									class="img_responsive_max wow bounceIn" alt="" />
							</div>
							<div class="text_left">
								<h3>Dr. Erik Bana</h3>
								<span>M.D. Pediatrics </span>
								<p>“ Lorem Ipsum is simply dummy text of the printing and
									typesetting industry. Lorem Ipsum has been the industry's
									standard dummy text ever since the 1500s, when an unknown print
									took a galley of type and scrambled it to make a type specimen
									book. ”</p>
								<a href="#">join us as a doctor</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--happy patient section-->

	<section class="happpatient_sec_dv">
		<div class="container">
			<div class="happypatient_dv">
				<h4>Happy Patients</h4>
				<div class="bxslider">
					<div class="item_slide">
						<div class="patient_img_dv">
							<img src="images/testo-image.png" class="img_responsive_max"
								alt="" />
						</div>
						<div class="patient_para_dv">
							<p>
								Lorem Ipsum is simply dummy text of the printing and
								typesetting. Lorem Ipsum has been the industry's standard dummy
								text ever since the 1500s.<br> <br> Lorem Ipsum is
								simply dummy text of the printing and typesetting. Lorem Ipsum
								has been the industry's standard dummy text ever since the
								1500s.
							</p>
						</div>
						<div class="pateint_name_dv">
							<h6>WILL TURNER</h6>
						</div>
					</div>

					<div class="item_slide">
						<div class="patient_img_dv">
							<img src="images/testo-image.png" class="img_responsive_max"
								alt="" />
						</div>
						<div class="patient_para_dv">
							<p>
								Lorem Ipsum is simply dummy text of the printing and
								typesetting. Lorem Ipsum has been the industry's standard dummy
								text ever since the 1500s.<br> <br> Lorem Ipsum is
								simply dummy text of the printing and typesetting. Lorem Ipsum
								has been the industry's standard dummy text ever since the
								1500s.
							</p>
						</div>
						<div class="pateint_name_dv">
							<h6>WILL TURNER</h6>
						</div>
					</div>

					<div class="item_slide">
						<div class="patient_img_dv">
							<img src="images/testo-image.png" class="img_responsive_max"
								alt="" />
						</div>
						<div class="patient_para_dv">
							<p>
								Lorem Ipsum is simply dummy text of the printing and
								typesetting. Lorem Ipsum has been the industry's standard dummy
								text ever since the 1500s.<br> <br> Lorem Ipsum is
								simply dummy text of the printing and typesetting. Lorem Ipsum
								has been the industry's standard dummy text ever since the
								1500s.
							</p>
						</div>
						<div class="pateint_name_dv">
							<h6>WILL TURNER</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--happy patient section-->





	<!--pay per section start-->


	<div class="payper_session">
		<div class="container">

			<div class="paypersession_dv">

				<h4>Pay per session, no hidden fees</h4>

				<div class="pricing_table_row">

					<div class="col-sm-4 pricing_table_column blue_brd">
						<div class="prcing_table_inner_dv">
							<h5>Medical & Pediatrics</h5>
							<h2>$40</h2>
							<h6>Per SESSION</h6>

							<a href="#">lEARN MORE</a>

						</div>
					</div>


					<div class="col-sm-4 pricing_table_column green_brd">
						<div class="prcing_table_inner_dv">
							<h5>Psychologists</h5>
							<h2>$40</h2>
							<h6>Per SESSION</h6>

							<a href="#">lEARN MORE</a>

						</div>
					</div>



					<div class="col-sm-4 pricing_table_column">
						<div class="prcing_table_inner_dv">
							<h5>Lactation Consultants</h5>
							<h2>$40</h2>
							<h6>Per SESSION</h6>

							<a href="#">lEARN MORE</a>

						</div>
					</div>


				</div>
			</div>
		</div>
	</div>
	<!--pay per section end-->






























	<!-- ==============================testomonial section end here -->

	<!-----------------------------------accept insurence card section -->

	<section class="accpt_i_section_bg">



		<div class="container">

			<div class="accep_grn_bg_dv">

				<h3>We also accept insurance cards</h3>

				<h4>Get help from real doctor now</h4>

				<a href="#">BOOK AN APPOINTMENT</a>

			</div>
		</div>
	</section>
@endsection

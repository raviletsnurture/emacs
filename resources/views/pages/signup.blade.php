<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Sign up</title>
	<!-- favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('images/favicon.ico') }}">

	<!-- metis menu -->
	<link href="{{ URL::to('css/metisMenu.css') }}" rel="stylesheet">

	<!-- jassney-Bootstrap -->
	<link href="{{ URL::to('css/jasny-bootstrap.css') }}" rel="stylesheet">
	<link href="{{ URL::to('css/bootstrap-switch.css') }}" rel="stylesheet">
	<!--stylish radio-->
	<link rel="stylesheet" href="{{ URL::to('skins/flat/red.css') }}" type="text/css" media="all">
	<!-- Bootstrap -->
	<link href="{{ URL::to('css/datepicker.css') }}" rel="stylesheet">
	<link href="{{ URL::to('css/bootstrap.css') }}" rel="stylesheet">
	<!-- bootstrap select -->
	<link href="{{ URL::to('css/bootstrap-select.css') }}" rel="stylesheet">

	<!-- animation css -->
	<link href="{{ URL::to('css/hover.css') }}" rel="stylesheet">
	<link href="{{ URL::to('css/animate.css') }}" rel="stylesheet">
	<!-- animation css -->

	<!-- font awesome -->
	<link href="{{ URL::to('css/font-awesome.css') }}" rel="stylesheet">
	<link href="{{ URL::to('css/chart.css') }}" rel="stylesheet">
	<!-- custom css -->
	<link href="{{ URL::to('css/admin_default.css') }}" rel="stylesheet">
	<link href="{{ URL::to('css/style.css') }}" rel="stylesheet">
	<link href="{{ URL::to('css/admin_responsive.css') }}" rel="stylesheet">

	<!-- google fonts -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700'	rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
	    <![endif]-->
</head>
<body class="">

	<!--upper header-->
	<header class="header_bg">
		<div class="container-fluid">
			<div class="login_logo_dv">
				<img src="{{ URL::to('images/login_logo.png') }}" class="img_responsive_max" alt="" />
			</div>
			<div class="signin_text_right">
				<a href="#"> SIGN IN</a>
			</div>
		</div>
	</header>
	<div class="login_wrapper">
		<div class="login_container">

			<form name="frmSignUp" id="frmSignUp">
				<input type="hidden" name="_token" value="ws">
				<input type="hidden" name="have_members" value="1">
			<div class="login_high_width">
				<div class="signup_container " id="step_1">
					<div class="login_heading">
						<h1>Sign Up</h1>
						<p>Contact Details</p>
						<div class="step_bg_dv">
							<span class="active step1" id="badge_step_1">1</span> <span
								class="step2" id="badge_step_2">2</span> <span class="step3"
								id="badge_step_3">3</span>
						</div>
					</div>

					<ul class="signup_list">
						<li class="dvd_2">
							<label> <input name="firstname" type="text" class="form-control" placeholder="First Name"> </label> <label> <input name="lastname" type="text"	class="form-control" placeholder="Last Name"> </label>
						</li>
						<li class="radio_dv">
							<div class="radiobtn_holder icheck_radio">
								<span>
									<input type="radio" name="gender" class="" id="Male" value="male" checked="checked">
									<label for="Male">Male</label> </span>
								<span>
									<input type="radio"	name="gender" class="" id="Female" value="female"> <label for="Female">Female</label> </span>
								<span>
									<input type="radio" name="gender" class="" id="Other" value="other"> <label for="Other">Other</label> </span>
							</div>
						</li>

						<li><input type="text" class="form-control" placeholder="Email" name="email" id="email">
						</li>
						<li class="dvd_2"><label> <input name="password" type="password" id="password"
								class="form-control" placeholder="Password"> </label> <label> <input name="confirmPassword"
								type="password" class="form-control"
								placeholder="Confirm Password"> </label>
						</li>
						<li><input name="address_line1" type="text" class="form-control"
							placeholder="Address Line 1 *">
						</li>
						<li><input name="address_line2" type="text" class="form-control"
							placeholder="Address Line 2">
						</li>
						<li class="dvd_3"><label> <input name="city" type="text" class="form-control"
								placeholder="CIty *"> </label> <label> <select name="country"
								class="selectpicker">
									<option value="">Country *</option>
									@foreach($countryData as $country)
										<option value="{{$country->country_name}}">{{$country->country_name}}</option>
									@endforeach
							</select> </label> <label> <input name="zip" type="text"
								class="form-control" placeholder="Zip"></label>
						</li>
						<li class="next_btn">
							<button class="step_btn1">
								Next <i class="fa fa-angle-right"></i>
							</button>
						</li>
					</ul>
				</div>


				<div class="signup_container" id="step_2">

					<div class="login_heading">
						<h1>Sign Up</h1>
						<p>Insurance & CC Details</p>
						<div class="step_bg_dv">
							<span class="">1</span> <span class="step2 active">2</span> <span
								class="step3">3</span>
						</div>
					</div>


					<ul class="signup_list">
						<li class="swithc ">Do you have Insurance ? <span class="right">
							<input name="has_insurance" id="has_insurance" type="checkbox"> </span>
						</li>
						<li><select class="form-control" name="insurance_provider_name" id="insurance_provider_name">
								<option value="">Select Provider 1 *</option>
								<option value="LIC">LIC</option>
								<option value="HDFC">HDFC</option>
								<option value="ICICI">ICICI</option>
						</select>
						</li>
						<li class="dvd_2"><label> <input name="policy_id" type="text" class="form-control"
								placeholder="Policy ID *"> </label> <label> <input name="insurance_member_id" type="text"
								class="form-control" placeholder="Member ID *"> </label>
						</li>
						<li>
						  <div class="bg_file_input">
						    <label for="insurance_card_image_picker" class="fileSelectButton">
						      <img src="{{ URL::to('images/upload_f_btn.png') }}" alt="" />
						    </label>
						      <input name="insurance_card_image_picker" for="insurance_card_image" id="insurance_card_image_picker" type="file"	style="display: none">
									<input type="hidden" name="insurance_card_image" value="" id="insurance_card_image">
						      <span id="insurance_card_filenameplaceholder" class="txt"></span>
						  </div>
						</li>
						<li>
						  <div class="bg_file_input">
						    <label for="credit_card_image_picker" class="fileSelectButton">
						      <img src="{{ URL::to('images/upload_f_btn.png') }}" alt="" />
						    </label>
						      <input name="credit_card_image_picker" for="credit_card_image" id="credit_card_image_picker" type="file"	style="display: none">
									<input type="hidden" name="credit_card_image" value="" id="credit_card_image">
						      <span id="credit_card_filenameplaceholder" class="txt"></span>
						  </div>
						</li>
						<li><input type="text" class="form-control" name="credit_card_number"
							placeholder="Enter CC Number * ">
						</li>
						<li class="dvd_2"><label> <select class="form-control" name="expiry_month">
									<option value="">Expiry MM *</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
							</select> </label> <label> <select class="form-control" name="expiry_year">
									<option value="">Expiry YY *</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2016</option>
							</select> </label>
						</li>
						<li class="next_btn_dvd_2">
							<button class="back step_btn2_back">
								<i class="fa fa-angle-left"></i> Back
							</button>
							<button class="step_btn2_next">
								Next <i class="fa fa-angle-right"></i>
							</button>
						</li>
					</ul>
				</div>

				<div class="signup_container" id="step_3">

					<div class="login_heading">
						<h1>Sign Up</h1>
						<p>Members (Only below 18 years of Age)</p>
						<div class="step_bg_dv">
							<span class="">1</span> <span class="step2">2</span> <span
								class="step3 active">3</span>
						</div>
					</div>

				</form>
					<ul class="signup_list" id="frmAddMemberDiv">
						<li><input name="first_name" type="text" class="form-control"
							placeholder="First Name " id="first_name">
						</li>
						<li><input name="last_name" type="text" class="form-control"
							placeholder="Last Name " id="last_name">
						</li>
						<li class="clander_list">
							<div class="input-group date datetimepicker1">
								<input name="dob" type="text" class="form-control memberDOB" id="datetimepicker1" /> <span
									class="input-group-addon"> <span
									class="glyphicon glyphicon-calendar"></span> </span>
							</div>
						</li>
						<li class="radio_dv">
							<div class="radiobtn_holder icheck_radio">
								<span> <input value="male" type="radio" name="gender" class="" checked="checked"> <label
									for="Male">Male</label> </span> <span> <input type="radio" value="female"
									name="gender" class=""> <label for="Female">Female</label> </span>
								<span> <input value="other" type="radio" name="gender" class=""> <label
									for="Other">Other</label> </span>
							</div>
						</li>
						<li class="addmemeber">
							<button id="btnAddMember">Add Member</button>
						</li>
					</ul>
					<div class="adding_members_dv">
						<div class="adding_member_heading">
							<h3>Members</h3>
						</div>
						<ul class="adding_list">
						</ul>
						<ul class="lstbnts">
							<li class="next_btn_dvd_2">
								<button class="back step_btn3_back">
									<i class="fa fa-angle-left"></i> Back
								</button>
								<button id="btnSubmit">Create Account</button>
							</li>
						</ul>
					</div>

					<div style="clear: both; padding-top: 60px;"></div>

				</div>

			</div>

		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="{{ URL::to('js/jquery.min.js') }}"></script>
	<script src="{{ URL::to('js/bootbox.min.js') }}"></script>
	<!-- bootstrap select -->
	<script src="{{ URL::to('js/bootstrap-select.min.js') }}"></script>

	<!--bootstrap calendar-->
	<script src="{{ URL::to('js/moment-with-locales.js') }}"></script>
	<script src="{{ URL::to('js/bootstrap-datepicker.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap-timepicker.js') }}"></script>

	<!--stylish radio-->
	<script src="{{ URL::to('js/icheck.js') }}"></script>
	<!-- pie chart-->
	<script src="{{ URL::to('js/jquery.flot.js') }}"></script>
	<script src="{{ URL::to('js/jquery.flot.pie.js') }}"></script>

	<!--<script type="text/javascript">

	$(function() {

		var d1 = [];
		for (var i = 0; i < 14; i += 0.5) {
			d1.push([i, Math.sin(i)]);
		}

		var d2 = [[0, 3], [4, 8], [8, 5], [9, 13]];

		// A null signifies separate line segments

		var d3 = [[0, 12], [7, 12], null, [7, 2.5], [12, 2.5]];

		$.plot("#placeholder", [ d1, d2, d3 ]);

		// Add the Flot version string to the footer

		$("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
	});

	</script>-->

	<!-- metsi menu-->
	<script src="{{ URL::to('js/metisMenu.js') }}"></script>

	<!-- animation-->
	<script src="{{ URL::to('js/wow.js') }}"></script>

	<!-- video lightbox-->
	<script type="text/javascript" src="html5lightbox/html5lightbox.js') }}"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->

	<!--bx slider-->
	<script src="{{ URL::to('js/jquery.bxslider.js') }}"></script>

	<!--custom script-->
	<script src="{{ URL::to('js/custom.js') }}"></script>

	<!--bootstrap-->
	<script src="{{ URL::to('js/bootstrap.js') }}"></script>

	<!--Jasney bootstrap-->
	<script src="{{ URL::to('js/jasny-bootstrap.js') }}"></script>
	<script src="{{ URL::to('js/bootstrap-switch.js') }}"></script>

	<!-- form Submission js -->
	<script src="{{ URL::to('js/frmSubmission.js') }}"></script>

	<script src="{{ URL::to('js/jquery.validate.min.js') }}"></script>
	<script src="{{ URL::to('js/additional-methods.js') }}"></script>

	<script src="{{ URL::to('js/validationCode-bak.js') }}"></script>

</body>
</html>

@extends('layouts.loginedMaster')
@section('content')
<div class="" id="wrapper">
  <div class="content">
    <div class="admin_tittle_name">
      <h3>MEDICAL INQUIRY</h3>
    </div>
    <div class="row medical_condition_row">

      <!--chart_column-->
      <form name="frmInquiry" id="frmInquiry">
        <input type="hidden" value="{{ csrf_token() }}" id="token" name="token">
        <input type="hidden" value="{{Auth::user()->user_type}}" name="msg_type">
        <input type="hidden" value="{{Auth::user()->user_id}}" name="user_id">
        <input type="hidden" name="parent_id" id="parent_id">

      <div class="col-sm-6 medical_dondition_column">
        <div class="conditionla_heading">
          <h1 id="queryHeading">Type Query</h1>
        </div>
        <div class="new_prescipton_dv">
          <div class="medical-inquiryview">

          <ul class="pre_listing">
            <li>
              <input class="form-control" placeholder="Query Title *" type="text" name="query_title">
            </li>
            <li>
              <select class="selectpicker" name="member_id" id="member_id">
                <option value="">Select Member</option>
                @foreach($memberData as $member)
                  <option value="{{ $member->member_id }}">{{ $member->first_name }} {{ $member->last_name }}</option>
                @endforeach
              </select>
            </li>
            <li>
              <textarea name="text" class="form-control" placeholder="Type Your Query *" type="text"></textarea>
            </li>
            <li>
              <input type="button" class="btn1 can" value="Cancel" />
              <input type="button" class="btn1 save" value="Send" id="btnSend"/>
            </li>
          </ul>
        </div>
        </div>
      </div>
    </form>
      <!--Prescribed Medicines-->
      <div class="col-sm-6 Prescribed_columnn ">
        <div class="prescribe_holder">
          <div class="prescribe_heading">
            <h1>Queries</h1>
            <!-- <ul>
              <li class=""><a href="#" class="active">By Date</a> | <a href="#"> By Doctor</a></li>
            </ul> -->
          </div>

          <!--prescribe_listing_db-->
          <div class="prescribe_listing_db medical">
            <ul>
              @foreach($returnData as $inquiry)
              @if(isset($inquiry->query_title) && !empty($inquiry->query_title))
                <li><a href="javascript:void(0);" class="inquiryListItem" data="{{$inquiry->msg_id}}">{{$inquiry->query_title}}</a><span>{{date('d-M-Y',strtotime($inquiry->created_at))}}</span></li>
              @endif
              @endforeach
            </ul>
          </div>
          <!--prescribe_listing_db-->
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	window.onload=function(){
		$('.selectpicker').selectpicker();
	};
  $(document).ready(function(){
    $("#frmInquiry").validate({
        onfocusout: false,
          invalidHandler: function(form, validator) {
              var errors = validator.numberOfInvalids();
              if (errors) {
                  validator.errorList[0].element.focus();
              }
          },
        ignore: [],
        debug: false,
        rules: {
          text:{
            required: true
          },
        },
        query_title:{
          text:{
            required: 'Please enter content',
          },
        },
      });



    $(document).on("click","#btnReply",function(){
      if($("#frmInquiry").valid()){
        var dataString = $("#frmInquiry").serialize();
        console.log(dataString);
        $.ajax({
          type: "POST",
          url: "{{url()}}/api/query",
          data: dataString,
          dataType: "json",
          beforeSend:function(){

          },
          success:function(data){
            if(data.status){
              window.location.reload();
              $("#frmInquiry").find("input[type='text']").val("");
              $("#frmInquiry").find("#member_id").val("");
              $("#frmInquiry").find("textarea").val("");
            }
          }
        });
      }
    });

    $("#frmInquiry").validate({
        onfocusout: false,
          invalidHandler: function(form, validator) {
              var errors = validator.numberOfInvalids();
              if (errors) {
                  validator.errorList[0].element.focus();
              }
          },
        ignore: [],
        debug: false,
        rules: {
          query_title:{
            required: true,
          },
          text:{
            required: true
          },
        },
        query_title:{
          firstname:{
            required: 'Please enter first name',
          },
          text:{
            required: 'Please enter content',
          },
        },
      });



    $("#btnSend").on("click",function(){
      if($("#frmInquiry").valid()){
        var dataString = $("#frmInquiry").serialize();
        $.ajax({
          type: "POST",
          url: "{{url()}}/api/query",
          data: dataString,
          dataType: "json",
          beforeSend:function(){

          },
          success:function(data){
            if(data.status){
              window.location.reload();
              $("#frmInquiry").find("input[type='text']").val("");
              $("#frmInquiry").find("#member_id").val("");
              $("#frmInquiry").find("textarea").val("");
            }
          }
        });
      }
    });
    $(".inquiryListItem").on("click",function(){
      // set parent_id
      var query_id = $(this).attr("data");
      $("#parent_id").val(query_id);

      var dataString = "query_id="+query_id;
      $.ajax({
        type: "POST",
        url: "details",
        data: dataString,
        dataType: "json",
        beforeSend:function(){

        },
        success:function(data){
          console.log(data);
          if(data!="0"){
            $(".pre_listing").html(data.html);
            $(".pre_listing").append("<li><input type='button' class='btn1 can myBtn' value='Cancel' id='btnCancel'/></li>");
            $("#queryHeading").text("Query Details");
          }else {
            $(".pre_listing").html("");
            $(".pre_listing").html("No content found");
          }
        }
      })
    });

    $(document).on("click","#btnCancel",function(){
      window.location.reload();
    });


  });
</script>
@stop

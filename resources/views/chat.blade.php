<html>
<head>
  <title>LetsNurture - Video chat example</title>
  <link rel="stylesheet" href="style.css">
  <style type="text/css">
  	#step3 { display: none; }
  </style>
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
  <script src="http://cdn.peerjs.com/0.3/peer.min.js"></script>
  <link type="text/css" href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
  <script>

    // Compatibility shim
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

    // PeerJS object
    //var peer = new Peer({key: 'ck6kfap96c62bj4i'});
    
    var peer = new Peer({
    	  // Set API key for cloud server (you don't need this if you're running your
    	  // own.
   	  key: 'ck6kfap96c62bj4i',

   	  // Set highest debug level (log everything!).
   	  debug: 3,

   	  // Set a logging function:
   	  logFunction: function() {
   	    var copy = Array.prototype.slice.call(arguments).join(' ');
   	    $('.log').append(copy + '<br>');
   	  }
   	});
    	  
   	var connectedPeers = {};
   	
	peer.on('open', function(id) {
		alert(id);
		$('#my-id').text(peer.id); 
		  console.log('My peer ID is: ' + id);
	});
	peer.on('connection', connect);
	peer.on('error', function(err) {
		  console.log(err);
		});
	
	function connect(c) {
		  // Handle a chat connection.
		  if (c.label === 'chat') {
		    var chatbox = $('<div></div>').addClass('connection').addClass('active').attr('id', c.peer);
		    var header = $('<h1></h1>').html('Chat with <strong>' + c.peer + '</strong>');
		    var messages = $('<div><em>Peer connected.</em></div>').addClass('messages');
		    chatbox.append(header);
		    chatbox.append(messages);
		 
		    // Select connection handler.
		    chatbox.on('click', function() {
		      if ($(this).attr('class').indexOf('active') === -1) {
		        $(this).addClass('active');
		      } else {
		        $(this).removeClass('active');
		      }
		    });
		    $('.filler').hide();
		    $('#connections').append(chatbox);

		    c.on('data', function(data) {
		      messages.append('<div><span class="peer">' + c.peer + '</span>: ' + data +
		        '</div>');
		        });
		        c.on('close', function() {
		          alert(c.peer + ' has left the chat.');
		          chatbox.remove();
		          if ($('.connection').length === 0) {
		            $('.filler').show();
		          }
		          delete connectedPeers[c.peer];
		        });
		  } else if (c.label === 'file') {
		    c.on('data', function(data) {
		      // If we're getting a file, create a URL for it.
		      if (data.constructor === ArrayBuffer) {
		        var dataView = new Uint8Array(data);
		        var dataBlob = new Blob([dataView]);
		        var url = window.URL.createObjectURL(dataBlob);
		        $('#' + c.peer).find('.messages').append('<div><span class="file">' +
		            c.peer + ' has sent you a <a target="_blank" href="' + url + '">file</a>.</span></div>');
		      }
		    });
		  }
		  connectedPeers[c.peer] = 1;
		}
	
	$(document).ready(function() {
		  // Prepare file drop box.
		  var box = $('#box');
		  box.on('dragenter', doNothing);
		  box.on('dragover', doNothing);
		  box.on('drop', function(e){
		    e.originalEvent.preventDefault();
		    var file = e.originalEvent.dataTransfer.files[0];
		    eachActiveConnection(function(c, $c) {
		      if (c.label === 'file') {
		        c.send(file);
		        $c.find('.messages').append('<div><span class="file">You sent a file.</span></div>');
		      }
		    });
		  });
		  function doNothing(e){
		    e.preventDefault();
		    e.stopPropagation();
		  }

		  // Connect to a peer
		  $('#connect').click(function() {
		    var requestedPeer = $('#rid').val();
		    
		    var call = peer.call($('#rid').val(), window.localStream);
	        step3(call);
	        
		    if (!connectedPeers[requestedPeer]) {
		      // Create 2 connections, one labelled chat and another labelled file.
		      var c = peer.connect(requestedPeer, {
		        label: 'chat',
		        serialization: 'none',
		        metadata: {message: 'hi i want to chat with you!'}
		      });
		      c.on('open', function() {
		        connect(c);
		      });
		      c.on('error', function(err) { alert(err); });
		      var f = peer.connect(requestedPeer, { label: 'file', reliable: true });
		      f.on('open', function() {
		        connect(f);
		      });
		      f.on('error', function(err) { alert(err); });
		    }
		    connectedPeers[requestedPeer] = 1;
		  });

		  // Close a connection.
		  $('#close').click(function() {
		    eachActiveConnection(function(c) {
		      c.close();
		    });
		  });

		  // Send a chat message to all active connections.
		  $('#send').submit(function(e) {
		    e.preventDefault();
		    // For each active connection, send the message.
		    var msg = $('#text').val();
		    eachActiveConnection(function(c, $c) {
		      if (c.label === 'chat') {
		        c.send(msg);
		        $c.find('.messages').append('<div><span class="you">You: </span>' + msg
		          + '</div>');
		      }
		    });
		    $('#text').val('');
		    $('#text').focus();
		  });

		  // Goes through each active peer and calls FN on its connections.
		  function eachActiveConnection(fn) {
		    var actives = $('.active');
		    var checkedIds = {};
		    actives.each(function() {
		      var peerId = $(this).attr('id');

		      if (!checkedIds[peerId]) {
		        var conns = peer.connections[peerId];
		        for (var i = 0, ii = conns.length; i < ii; i += 1) {
		          var conn = conns[i];
		          fn(conn, $(this));
		        }
		      }

		      checkedIds[peerId] = 1;
		    });
		  }

		  // Show browser version
		  $('#browsers').text(navigator.userAgent);
		});

		// Make sure things clean up properly.

		window.onunload = window.onbeforeunload = function(e) {
		  if (!!peer && !peer.destroyed) {
		    peer.destroy();
		  }
		};
	

    // Receiving a call
    peer.on('call', function(call){
      // Answer the call automatically (instead of prompting user) for demo purposes
      call.answer(window.localStream);
      step3(call);
    });
    
    peer.on('error', function(err){
      alert(err.message);
      // Return to step 2 if error occurs
      step2();
    });

    // Click handlers setup
    $(function(){
      $('#make-call').click(function(){
        // Initiate a call!
        var call = peer.call($('#callto-id').val(), window.localStream);

        step3(call);
      });

      $('#end-call').click(function(){
        window.existingCall.close();
        step2();
      });

      // Retry if getUserMedia fails
      $('#step1-retry').click(function(){
        $('#step1-error').hide();
        step1();
      });

      // Get things started
      step1();
    });

    function step1 () {
      // Get audio/video stream
      navigator.getUserMedia({audio: true, video: true}, function(stream){
        // Set your video displays
        $('#my-video').prop('src', URL.createObjectURL(stream));

        window.localStream = stream;
        step2();
      }, function(){ $('#step1-error').show(); });
    }

    function step2 () {
      $('#step1, #step3').hide();
      $('#step2').show();
    }

    function step3 (call) {
      // Hang up on an existing call if present
      if (window.existingCall) {
        window.existingCall.close();
      }

      // Wait for stream on the call, then set peer video display
      call.on('stream', function(stream){
        $('#their-video').prop('src', URL.createObjectURL(stream));
      });

      // UI stuff
      window.existingCall = call;
      $('#their-id').text(call.peer);
      call.on('close', step2);
      $('#step1, #step2').hide();
      $('#step3').show();
    }

  </script>


</head>

<body>

  <div class="pure-g">

      <!-- Video area -->
      <div class="pure-u-2-3" id="video-container">
        <video id="their-video" autoplay></video>
        <video id="my-video" muted="true" autoplay></video>
      </div>

      <!-- Steps -->
      <div class="pure-u-1-3">
        <h2>LetsNurture Video Chat</h2>

        <!-- Get local audio/video stream -->
        <div id="step1">
          <p>Please click `allow` on the top of the screen so we can access your webcam and microphone for calls.</p>
          <div id="step1-error">
            <p>Failed to access the webcam and microphone. Make sure to run this demo on an http server and click allow when asked for permission by the browser.</p>
            <a href="#" class="pure-button pure-button-error" id="step1-retry">Try again</a>
          </div>
        </div>

        <!-- Make calls to others -->
        <div id="step2">
          <p>Your id: <span id="my-id">...</span></p>
          <p>Share this id with others so they can call you.</p>
          <h3>Make a call</h3>
          <div class="pure-form">
            <input type="text" placeholder="Call user id..." id="callto-id">
            <a href="#" class="pure-button pure-button-success" id="make-call">Call</a>
          </div>
        </div>

        <!-- Call in progress -->
        <div id="step3">
          <p>Currently in call with <span id="their-id">...</span></p>
          <p><a href="#" class="pure-button pure-button-error" id="end-call">End call</a></p>
        </div>
      </div>
  </div>

Connect to a peer: <input type="text" id="rid" placeholder="Someone else's id"><input class="button" type="button" value="Connect" id="connect"><br><br>

    <form id="send">
      <input type="text" id="text" placeholder="Enter message"><input class="button" type="submit" value="Send to selected peers">
    </form>
    <button id="close">Close selected connections</button>
  </div>

  <div id="wrap"><div id="connections"><span class="filler">You have not yet
        made any connections.</span></div>
    <div class="clear"></div></div>

  <div id="box" style="background: #fff; font-size: 18px;padding:40px 30px; text-align: center;">
    Drag file here to send to active connections.
  </div>
</body>
</html>
@extends('layouts.loginedMaster')
@section('content')
<!--main content-->
<div class="" id="wrapper">
  <div class="content after_login_dv">
    <div class="admin_tittle_name after_log">
      <h3 class="shdeule_hd text_upper">Schedule an Appointment</h3>
    </div>
    <div class="shedule_an_apportment_continer">
      <div class="row grid_boxes_row">
        <div class="col-sm-6">
          <div class="thumbnail">
            <div class="caption">
              <h4>Medical doctor</h4>
            </div>
            <div class="dctr_hedig">
              <h3>Medical doctor</h3>
            </div>
            <img src="{{ URL::asset('images/appoinment_img_1.png') }}"  class="img_responsive_100"  alt=""/></div>
        </div>
        <div class="col-sm-6">
          <div class="thumbnail">
            <div class="caption">
              <h4>Pregnancy &amp; newborns</h4>
            </div>
            <div class="dctr_hedig">
              <h3>Pregnancy &amp; newborns</h3>
            </div>
            <img src="{{ URL::asset('images/appoinment_img_2.png') }}"  class="img_responsive_100"  alt=""/> </div>
        </div>
        <div class="col-sm-6">
          <div class="thumbnail">
            <div class="caption">
              <h4>Psychology</h4>
            </div>
            <div class="dctr_hedig">
              <h3>Psychology</h3>
            </div>
            <img src="{{ URL::asset('images/appoinment_img_3.png') }}"  class="img_responsive_100"  alt=""/> </div>
        </div>
        <div class="col-sm-6">
          <div class="thumbnail">
            <div class="caption">
              <h4>pediatrics</h4>
            </div>
            <div class="dctr_hedig">
              <h3>pediatrics</h3>
            </div>
            <img src="{{ URL::asset('images/appoinment_img_4.jpg') }}"  class="img_responsive_100"  alt=""/> </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
